package com.bb.jboard.hpalm.v11_52;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedString;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.bb.jboard.admin.DataSource;
import com.bb.jboard.hpalm.Domain;
import com.bb.jboard.hpalm.Entity;
import com.bb.jboard.hpalm.HpAlmService;
import com.bb.jboard.hpalm.Project;
import com.bb.jboard.hpalm.Requirement;
import com.bb.jboard.hpalm.Run;
import com.bb.jboard.hpalm.TestInstance;

public class HpAlmServiceV11_52Test {

	private HpAlmServiceV11_52 almService;
	private DataSource dataSource;
	
	@Before
	public void before() throws JSONException {
		JSONObject dataSourceJson = new JSONObject();
		dataSourceJson.put("user", "david.brown");
		dataSourceJson.put("password", "D0wnT0wn!");
		dataSourceJson.put("url", "http://orasi-jira-alm.eastus.cloudapp.azure.com:8080/qcbin");
		dataSourceJson.put("domain", "DEFAULT");
		dataSourceJson.put("project", "DBrown1");
		dataSource = DataSource.from(dataSourceJson);
		almService = new HpAlmServiceV11_52(dataSource);
	}
	
	@Test
	@Ignore
	public void should_authenticate_unlogged_user() throws Exception {
		almService.authenticate();
		
		Project project2 = almService.getProject(dataSource);
//		write(project2.getDomain(), project2, "customization", "relations");
//		write(project2.getDomain(), project2, "requirement-coverages");
//		write(project2.getDomain(), project2, "tests");
		
//		for (Domain domain : domains) {
//			if (!domain.getName().equals("INTEGRATIONS")) {
//				continue;
//			}
//			for (Project project : domain.getProjects()) {
//				if (!project.getName().equals("DBrown_Agile")) {
//					continue;
//				}
//				for (String path : Arrays.asList("tests", "runs", "run-steps", "test-sets", "test-instances", "test-configs", "defects", "test-config-coverages", "requirements", "customization/extensions")) {
//					write(domain, project, path);
//				}
				
				
//				List<Run> runs = authenticationService.getRuns(project);
				
//			}
//		}
	}
	
	@Test
	@Ignore
	public void read_xml() throws Exception {
		@SuppressWarnings("unchecked")
		List<Entity> entities = (List<Entity>) new SafeSaxConverter().fromBody(new TypedFile("text/xml", new File("/home/mwanji/Dev/prj-work/toptal/swventures/hp-alm/data/DBrown_Agile/runs-INTEGRATIONS-DBrown_Agile.xml")), Entity.class);

		for (Entity entity : entities) {
			entity.getString("test-id", null);
		}
	}
	
	private void write(Domain domain, Project project, String path) {
		try {
			Response response = almService.getResponse(domain, project, path);
		
			InputStream in = response.getBody().in();
			try {
				File dir = new File("/home/mwanji/Dev/prj-work/toptal/swventures/hp-alm/data/" + project.getName() + "/20150819");
				dir.mkdirs();
//				FileOutputStream out = new FileOutputStream(new File(dir, path + "-" + domain.getName() + "-" + project.getName() + ".xml"));
//				IOUtils.copy(in, out);
				
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.transform(new StreamSource(in), new StreamResult(new File(dir, path + "-" + domain.getName() + "-" + project.getName() + ".xml")));
//				out.close();
			} finally {
				in.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void write(Domain domain, Project project, String path, String path2) {
		try {
			Response response = almService.getResponse(project, path, path2);
		
			InputStream in = response.getBody().in();
			try {
				File dir = new File("/home/mwanji/Dev/prj-work/toptal/swventures/hp-alm/data/" + project.getName());
				dir.mkdirs();
//				FileOutputStream out = new FileOutputStream(new File(dir, path + "-" + domain.getName() + "-" + project.getName() + ".xml"));
//				IOUtils.copy(in, out);
				
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.transform(new StreamSource(in), new StreamResult(new File(dir, path + "-" + path2 + "-" + domain.getName() + "-" + project.getName() + ".xml")));
//				out.close();
			} finally {
				in.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@Ignore
	public void simple_domains() throws Exception {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Domains><Domain Name=\"BOOTCAMP\"><Projects><Project Name=\"BPT_Training_RT\"/></Projects></Domain><Domain Name=\"ORASI_INTERNAL\"><Projects><Project Name=\"OrasiBootCamp\"/><Project Name=\"STM_Demo\"/><Project Name=\"STM_Training\"/><Project Name=\"Orasi_Internal_Sandbox\"/></Projects></Domain><Domain Name=\"INTEGRATIONS\"><Projects><Project Name=\"DBrown2\"/><Project Name=\"DBrown_Agile\"/><Project Name=\"Footprints_Demo\"/><Project Name=\"DBrown1\"/></Projects></Domain></Domains>";
		Method method = SafeSaxConverter.class.getDeclaredMethod("readDomains", TypedInput.class);
		method.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<Domain> domains = (List<Domain>) new SafeSaxConverter().fromBody(new TypedString(xml), method.getGenericReturnType());
		
		System.out.println(domains);
	}
	
	@Test
	@Ignore
	public void simple_defects() throws Exception {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Entities TotalResults=\"2\"><Entity Type=\"defect\"><Fields><Field Name=\"has-change\"><Value/></Field><Field Name=\"planned-closing-ver\"><Value/></Field></Fields></Entity><Entity Type=\"defect\"><Fields><Field Name=\"has-change\"><Value/></Field></Fields></Entity></Entities>";
		
//		List<Entity> defects = serializer.read(Entities.class, xml);
//		
//		System.out.println(defects);
	}
	
	@Test
	@Ignore
	public void get_runs_with_safe_sax() throws Exception {
		almService.authenticate();
		Project project = almService.getProject(dataSource);
		List<Run> runs = almService.getRuns(new Date(), null, "", project);
		for (Run run : runs) {
			System.out.println("Run: " + run.getExecutionDate() + " - " + run.getTestId());
		}
		for (Requirement requirement : almService.getRequirements(project)) {
			System.out.println("Requirement: " + requirement.getName());
		}
		for (com.bb.jboard.hpalm.Test test : almService.getTests(project, "")) {
			System.out.println("Test: " + test.getLastModifiedDate() + " - " + test.getExecStatus());
		}
	}

	@Test
	public void get_test_instances() throws Exception {
		almService.authenticate();
		Project project = almService.getProject(dataSource);
		List<TestInstance> testInstances = almService.getTestInstances(new Date(), null, project, Collections.<String>emptySet());
			
		for (TestInstance testInstance : testInstances) {
			System.out.println(testInstance.getExecDate() + " / " + testInstance.getStatus());
		}
	}
	
	@Test
	public void should_get_test_case_readiness_data() throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date now = cal.getTime();
		Date start = DateUtils.truncate(now, Calendar.DATE);
		Date end = DateUtils.ceiling(now, Calendar.DATE);

		almService.authenticate();

		try {
			Project project = almService.getProject(dataSource);
			List<com.bb.jboard.hpalm.Test> tests = almService.getTests(start, end, "status[\"Design\"or\"Ready\"]", project);

			System.out.println("_*_*_*_*_*_*_*_*_*_*_*_*_*_");
			for (final com.bb.jboard.hpalm.Test test : tests) {
				System.out.println(test.getId() + " / " + test.getExecStatus().getXmlValue() + " / " + test.getStatus() + " / " + test.getLastModifiedDate());
			}
			System.out.println("_*_*_*_*_*_*_*_*_*_*_*_*_*_");
		} finally {
			almService.logout();
		}
	}

	@After
	public void after() {
		almService.logout();
	}
}
