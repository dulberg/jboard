package it.com.bb.jboard.rest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import com.bb.jboard.rest.JboardResource;
import com.bb.jboard.rest.UserStoryTestStatusResponse;

@RunWith(AtlassianPluginsTestRunner.class)
public class JBoardResourceTest {
	private final ApplicationProperties applicationProperties;
	private final JboardResource jboardResource;

	public JBoardResourceTest(ApplicationProperties applicationProperties, JboardResource jboardResource) {
		this.applicationProperties = applicationProperties;
		this.jboardResource = jboardResource;
	}
	
	@Test
	public void should_get_user_story_test_data() throws SearchException {
		UserStoryTestStatusResponse userStoryTestStatusResponse = (UserStoryTestStatusResponse) jboardResource.getUserStoryTestStatus("", "").getEntity();
		
		JSONObject jsonObject = new JSONObject(userStoryTestStatusResponse);
		
		Assert.assertEquals("{}", jsonObject.toString());
	}
}