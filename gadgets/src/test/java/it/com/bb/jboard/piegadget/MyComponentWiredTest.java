package it.com.bb.jboard.piegadget;

import org.junit.Ignore;

import com.atlassian.sal.api.ApplicationProperties;
import com.bb.jboard.piegadget.MyPluginComponent;

//@RunWith(AtlassianPluginsTestRunner.class)
@Ignore
public class MyComponentWiredTest
{
    private final ApplicationProperties applicationProperties;
    private final MyPluginComponent myPluginComponent;

    public MyComponentWiredTest(ApplicationProperties applicationProperties,MyPluginComponent myPluginComponent)
    {
        this.applicationProperties = applicationProperties;
        this.myPluginComponent = myPluginComponent;
    }
}