package ut.com.bb.jboard.piegadget;

import org.junit.Test;
import com.bb.jboard.piegadget.MyPluginComponent;
import com.bb.jboard.piegadget.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}