package ut.com.bb.jboard.rest;

import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Test;
import org.mockito.Mockito;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.bb.jboard.admin.AdminServlet;
import com.bb.jboard.rest.JboardResource;
import com.bb.jboard.rest.UserStoryTestStatusResponse;

public class JboardResourceTest {
	
	@Test
	public void should_get_from_all_endpoints() throws JSONException, JAXBException, SearchException {
		PluginSettingsFactory pluginSettingsFactory = Mockito.mock(PluginSettingsFactory.class);
		PluginSettings pluginSettings = Mockito.mock(PluginSettings.class);
		JSONObject dataSourceJson = new JSONObject();
		dataSourceJson.put("user", "david.brown");
		dataSourceJson.put("password", "D0wnT0wn!");
		dataSourceJson.put("url", "http://orasi-jira-alm.eastus.cloudapp.azure.com:8080/qcbin");
		dataSourceJson.put("domain", "DEFAULT");
		dataSourceJson.put("project", "DBrown1");
		dataSourceJson.put("jiraKeyId", "user-01");

		Mockito.when(pluginSettingsFactory.createGlobalSettings()).thenReturn(pluginSettings);
		Mockito.when(pluginSettings.get(AdminServlet.DATA_SOURCE_KEYS + ":1")).thenReturn(dataSourceJson.toString());
		
		JboardResource jboardResource = new JboardResource(pluginSettingsFactory, null, null, null, null, null);
//		UserStoryTestStatusResponse userStoryTestStatusResponse = (UserStoryTestStatusResponse) jboardResource.getUserStoryTestStatus("1", "").getEntity();

		Marshaller marshaller = JAXBContext.newInstance(UserStoryTestStatusResponse.class).createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//		marshaller.marshal(userStoryTestStatusResponse, System.out);
		
//		Object manualTestActivity = jboardResource.getManualTestActivity("1", "GAD-2,GAD-3", "", "2015-01-01", "").getEntity();
//		Object testCaseReadiness = jboardResource.getTestCaseReadiness("1", "", "2015-01-01", "").getEntity();
//		Object testFailureAnalysis = jboardResource.getTestFailureAnalysis("1", "2015-01-01", "", "").getEntity();
		Object testPlanProgress = jboardResource.getTestPlanProgress("1", "", "", "2015-01-01", "").getEntity();
		
//		System.out.println(manualTestActivity);
//		System.out.println(testCaseReadiness);
//		System.out.println(testFailureAnalysis);
		System.out.println(new JSONObject((Map)testPlanProgress));
	}
}
