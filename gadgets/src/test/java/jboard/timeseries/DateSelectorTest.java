package jboard.timeseries;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.junit.Test;

public class DateSelectorTest {
	
	@Test
	public void should_filter_last_dates_of_days() throws Exception {
		DateSelector dailyDateSelector = DateSelector.create("1D");
		
		DateTime date = new DateTime(2015, DateTimeConstants.OCTOBER, 22, 12, 0);
		DateTime lastOfPreviousDay = date.minusDays(1);
		List<Date> dates = Arrays.asList(date.toDate(), date.minusHours(2).toDate(), lastOfPreviousDay.toDate(), date.minusDays(1).minusHours(2).toDate());
		
		List<Date> filteredDates = dailyDateSelector.filter(dates);
		
		assertEquals(Arrays.asList(date.toDate(), lastOfPreviousDay.toDate()), filteredDates);
	}
	
	@Test
	public void should_filter_last_dates_of_hours() throws Exception {
		DateSelector dailyDateSelector = DateSelector.create("1H");
		
		DateTime date = new DateTime(2015, DateTimeConstants.OCTOBER, 22, 12, 0);
		List<Date> dates = Arrays.asList(date.toDate(), date.minusMinutes(2).toDate(), date.minusMinutes(62).toDate(), date.minusMinutes(90).toDate(), date.minusMinutes(119).toDate(), date.minusMinutes(123).toDate(), date.minusMinutes(145).toDate());
		
		List<Date> filteredDates = dailyDateSelector.filter(dates);
		
		assertEquals(Arrays.asList(date.toDate(), date.minusMinutes(62).toDate(), date.minusMinutes(123).toDate()), filteredDates);
	}
	
	@Test
	public void should_filter_last_dates_of_quarter_hours() throws Exception {
		DateSelector dailyDateSelector = DateSelector.create("15m");
		
		DateTime date = new DateTime(2015, DateTimeConstants.OCTOBER, 22, 12, 0);
		List<Date> dates = Arrays.asList(date.toDate(), date.minusMinutes(2).toDate(), date.minusMinutes(15).toDate(), date.minusMinutes(29).toDate(), date.minusMinutes(31).toDate(), date.minusMinutes(44).toDate(), date.minusMinutes(45).toDate(), date.minusMinutes(46).toDate(), date.minusMinutes(47).toDate());
		
		List<Date> filteredDates = dailyDateSelector.filter(dates);
		
		assertEquals(Arrays.asList(date.toDate(), date.minusMinutes(15).toDate(), date.minusMinutes(31).toDate(), date.minusMinutes(46).toDate()), filteredDates);
	}

	@Test
	public void should_filter_last_dates_of_minutes() throws Exception {
		DateSelector minuteDateSelector = DateSelector.create("1m");
		
		DateTime now = new DateTime(2015, DateTimeConstants.OCTOBER, 22, 12, 0, 0);
		DateTime tenSecondsEarlier = now.minusSeconds(10);
		DateTime oneMinuteEarlier = now.minusMinutes(1);
		List<Date> filteredDates = minuteDateSelector.filter(Arrays.asList(now.toDate(), tenSecondsEarlier.toDate(), oneMinuteEarlier.toDate()));

		assertEquals(Arrays.asList(now.toDate(), oneMinuteEarlier.toDate()), filteredDates);
	}
}
