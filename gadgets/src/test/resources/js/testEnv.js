var GADGET = {
  baseUrl: ""
};

$.fn.datePicker = function () {};

var AJS = {
  $: $
};

/*
var google = {
  load: function () {},
  setOnLoadCallback: function (callback) {
    callback();
  }
};
*/

var gadgets = {
  window: {
    adjustHeight: function () {}
  },
  util: {
    registerOnLoadHandler: function(callback) {
      callback();
    }
  },
	io: {
		// HTTP requests are stubbed against GADGET.test.api
		makeRequest: function (url, callback, options) {
		  var testUrl = Object.keys(GADGET.test.api).filter(function (candidate) {
		    return url.startsWith(candidate);
		  })[0];
		  callback(GADGET.test.api[testUrl]);
		}
	}
};

gadgets.Prefs = (function () {
	var STORE_KEY = "gadgets.test.prefs";
	var store = {};
	var get = function (key) {
		return store[key];
	};
	var getBool = function (key) {
		return get(key) || false;
	}
	var set = function (key, value) {
		store[key] = value;
		localStorage.setItem(STORE_KEY, JSON.stringify(store));
	}
	
	if (localStorage.getItem(STORE_KEY) !== null) {
		store = JSON.parse(localStorage.getItem(STORE_KEY));
	}
	
	return function () {
		return {
			set: set,
			setArray: set,
			getString: get,
			getBool: getBool,
			getInt: get,
			getFloat: get,
			getArray: get
		};
	};
})();

GADGET.test = {
	init: function () { // Sets up stubbed functions
		GADGET.loadChartLibrary = function (callback) {
		  callback();
		};
		
		GADGET.jsonParams = function () {
			return {};
		};
		
		GADGET.postParams = GADGET.jsonParams;
		
		if (window.URI !== undefined) {
			var src = new URI().search(true).script;
			if (src) {
				var script = document.createElement("script");
				script.setAttribute("src", "../../main/resources/js/" + src);
				document.getElementsByTagName("head")[0].appendChild(script);
			}
		}
	},
	api: {
		"/rest/jboard/1.0/api/manualTestActivity": {
			data: {
				dates: ["2015-05-17", "2015-05-18", "2015-05-19", "2015-05-20"],
				series: [
					{
						id: "testAmounts",
						data: [getRandomInt(0, 3), getRandomInt(6, 9), getRandomInt(3, 5), getRandomInt(6, 8)]
					}
				]
			}
		},
		"/rest/jboard/1.0/api/testFailureAnalysis": {
			data: {
				dates: ["2015-05-15", "2015-05-16", "2015-05-17", "2015-05-18", "2015-05-19"],
				series: [
					{
						id: "knownIssue",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					},
					{
						id: "newIssue",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					},
					{
						id: "none",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					},
					{
						id: "regression",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					}
				]
			}
		},
		"/rest/jboard/1.0/api/testFailureAnalysis?projectId=2": {
			data: {
				dates: ["2015-05-15", "2015-05-16", "2015-05-17", "2015-05-18", "2015-05-19"],
				series: [
					{
						id: "knownIssue",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					},
					{
						id: "newIssue",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					},
					{
						id: "none",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					},
					{
						id: "regression",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					}
				]
			}
		},
		"/rest/jboard/1.0/api/testCaseReadiness": {
			data: {
				dates: ["2015-05-15", "2015-05-16", "2015-05-17", "2015-05-18", "2015-05-19"],
				series: [
					{
						id: "design",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					},
					{
						id: "ready",
						data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
					}
				]
			}
		},
		"/rest/jboard/1.0/api/testPlanProgress": {
		  data: {
		    dates: ["2015-05-15", "2015-05-16", "2015-05-17", "2015-05-18", "2015-05-19"],
		    series: [
         {
           id: "No Run",
           data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
         },
         {
           id: "Failed",
           data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
         },
         {
           id: "Blocked",
           data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
         },
         {
           id: "Passed",
           data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
         }
        ]
		  }
		},
		"/rest/jboard/1.0/api/userStoryTestStatus": {
		  data: {
		    series:[
          { id: "neverRun", name: "Never Run" },
          { id: "blocked", name: "Blocked"  },
          { id: "failed", name: "Failed"  },
          { id: "passed", name: "Passed"  }
        ],
        data: [
          {
            name: "Regular members can save their preferences",
            key: "PRJ-1",
            neverRun: 2,
            blocked: 0,
            failed: 0,
            passed: 0
          },
          {
            name: "Restaurant members can post new entrees",
            key: "PRJ-2",
            neverRun: 2,
            blocked: 0,
            failed: 0,
            passed: 2
          },
          {
            name: "Gold members can search for villains",
            key: "PRJ-3",
            neverRun: 6,
            blocked: 0,
            failed: 0,
            passed: 4
          },
          {
            name: "Regular members can search for heroes",
            key: "PRJ-4",
            neverRun: 2,
            blocked: 3,
            failed: 0,
            passed: 0
          },
          {
            name: "Enable selection by strength, intelligence, etc.",
            key: "PRJ-5",
            neverRun: 2,
            blocked: 0,
            failed: 2,
            passed: 0
          },
          {
            name: "As a new customer, I want to oorder a meal",
            key: "PRJ-6",
            neverRun: 6,
            blocked: 0,
            failed: 4,
            passed: 2
          },
        ]
		  }
		},
		"/rest/jboard/1.0/admin/dataSources": {
		  "data": [
		    {
		      "id": "1234-5678",
		      "displayName": "DS 1"
		    }
      ]
		},
		"/rest/jboard/1.0/admin/globalSettings": {
		  "data": {
		    dataCollectionInterval: "15m"
		  }
		},
		"/rest/greenhopper/1.0/rapidviews/list": {
      "data": {
        "views": [
          {
            "id": "1",
            "name": "Agile Project 1"
          },
          {
            "id": "2",
            "name": "Agile Project 2"
          }
        ]
      }
		},
		"/rest/api/2/filter/favourite": {
      "data": [
        {
          "id": "1",
          "name": "Filter 1",
					"searchUrl": "/search?filter1"
        },
        {
          "id": "2",
          "name": "Filter 2",
					"searchUrl": "/search?filter2"
        }
      ]
		},
		"/search?filter1&fields=*all": {
			data: {
				issues: [
					{
						id: "1",
						key: "ISSUE-1.1",
						fields: {
							summary: "Summary 1.1"
						}
					},
					{
						id: "2",
						key: "ISSUE-1.2",
						fields: {
							summary: "Summary 1.2"
						}
					},
					{
						id: "3",
						key: "ISSUE-1.3",
						fields: {
							summary: "Summary 1.3"
						}
					}
				]
			}
		},
		"/search?filter2&fields=*all": {
			data: {
				issues: [
					{
						id: "4",
						key: "ISSUE-2.1",
						fields: {
							summary: "Summary 2.1"
						}
					},
					{
						id: "5",
						key: "ISSUE-2.2",
						fields: {
							summary: "Summary 2.2"
						}
					},
					{
						id: "6",
						key: "ISSUE-2.3",
						fields: {
							summary: "Summary 2.3"
						}
					}
				]
			}
		},
		"/rest/greenhopper/1.0/sprintquery/1?includeHistoricSprints=true&includeFutureSprints=true": {
			data: {
				sprints: [
					{
						id: "1",
						name: "Sprint 1.1"
					}
				]
			}
		},
		"/rest/greenhopper/1.0/sprintquery/2?includeHistoricSprints=true&includeFutureSprints=true": {
			data: {
				sprints: [
					{
						id: "2",
						name: "Sprint 2.1"
					},
					{
						id: "3",
						name: "Sprint 2.2"
					}
				]
			}
		},
		"/rest/api/2/project": {
			data: [
				{
					id: "1",
					name: "Project 1"
				},
				{
					id: "2",
					name: "Project 2"
				}
			]
		},
		"/rest/api/2/search?fields=*all&maxResults=50&jql=sprint%3D1": {
			data: {
				issues: [
					{
						id: "1",
						key: "SPRINT-1.1.1",
						fields: {
							summary: "Summary 1.1.1"
						}
					},
					{
						id: "2",
						key: "SPRINT-1.1.2",
						fields: {
							summary: "Summary 1.1.2"
						}
					},
					{
						id: "3",
						key: "SPRINT-1.1.3",
						fields: {
							summary: "Summary 1.1.3"
						}
					}
				]
			}
		},
		"/rest/api/2/search?fields=*all&maxResults=50&jql=sprint%3D2": {
			data: {
				issues: [
					{
						id: "4",
						key: "SPRINT-2.1.1",
						fields: {
							summary: "Summary 2.1.1"
						}
					},
				]
			}
		},
		"/rest/api/2/search?fields=*all&maxResults=50&jql=sprint%3D3": {
			data: {
				issues: [
					{
						id: "5",
						key: "SPRINT-2.2.1",
						fields: {
							summary: "Summary 2.2.1"
						}
					},
					{
						id: "6",
						key: "SPRINT-2.2.2",
						fields: {
							summary: "Summary 2.2.2"
						}
					}
				]
			}
		}
	}
};
  
// Returns a random integer between min (included) and max (excluded)
// Using Math.round() will give you a non-uniform distribution!
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
