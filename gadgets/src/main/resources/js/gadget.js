GADGET.jiraBaseUrl = GADGET.baseUrl + "/rest/api/2";
GADGET.jiraProjectBaseUrl = GADGET.jiraBaseUrl + "/project";
GADGET.jiraSearchBaseUrl = GADGET.jiraBaseUrl + "/search?fields=*all";
GADGET.agileBaseUrl = GADGET.baseUrl + "/rest/greenhopper/1.0";
GADGET.apiBaseUrl = GADGET.baseUrl + "/rest/jboard/1.0/hardcoded";
GADGET.adminBaseUrl = GADGET.baseUrl + "/rest/jboard/1.0/admin";

/**
 * @param apiEndpoint the last segment of the endpoint URL. Must begin with a slash.
 * @param chartConstructorProvider a function that returns the Google Charts constructor to use for this gadget.
 * @param onChartDataFetchedCallback a function that takes a Google Charts chart instance and a full HTTP response
 */
GADGET.init = function (apiEndpoint, chartConstructorProvider, onChartDataFetchedCallback) {

  var BAR_COLORS = {
    Passed: "#109618",
    Failed: "#dc3912",
    Blocked: "#ff9900",
    "Not Completed": "#3366cc",
    "No Run": "#bbbbbb"
  };


  function initEditor() {
    var app = AJS.$("#app");
    var messages = AJS.$('<div id="messages"></div>');
    
    if (document.getElementById("editorDiv") === null) {
      var editor = AJS.$(form("editorDiv"))
        .append(select("dataSourceId", "Data sources"))
        .append(select("filterId", "Filters"))
        .append(select("projectId", "Projects"))
        .append(select("interval", "Interval"))
        .append(input("fromDate", "From"))
        .append(input("toDate", "To"))
        .append(buttons());
      app.prepend(editor);
    }
    
    app.prepend(messages);
    
    if (document.getElementById("chartContainer") === null) {
      app.append('<div id="chartContainer">' +
        '<div id="chartDiv"></div>' +
          '<div id="chartOverlay">' +
            '<button id="editButton" type="button" class="aui-button aui-button-compact">Edit</button>' +
          '</div>' +
        '</div>');
    }
  }
  
  function loadChartLibrary(callback) {
    google.load("visualization", "1.0", {"packages":["corechart", "controls"]});
    google.setOnLoadCallback(initChart);
  };
  
  function onSaveClicked(event) {
    event.preventDefault();
    configuration.interval = intervalSelect.val();
    configuration.fromDate = fromDateInput.val();
    configuration.toDate = toDateInput.val();
    if (chart) {
      chart.clearChart();
    }
    GADGET.saveConfiguration(prefs, configuration);
    messagesContainer.empty();
    fetchChartData();
  }

  function onCancelClicked(event) {
    event.preventDefault();
    resetConfiguration();
		showChart();
  }
  
	function showEditor() {
		editorDiv.removeClass("hidden");
		chartDiv.addClass("hidden");
		gadgets.window.adjustHeight();
	}
  
	function showChart() {
		editorDiv.addClass("hidden");
		chartDiv.removeClass("hidden");
	}
	
	function showNone() {
	  editorDiv.addClass("hidden");
	  chartDiv.addClass("hidden");
	}

  function resetConfiguration() {
		configuration = GADGET.loadConfiguration(prefs);
		projectSelect.val(configuration.projectId);
		projectSelect.trigger("change");
		filterSelect.val(configuration.filterId);
		filterSelect.trigger("change");
		dataSourceSelect.val(configuration.dataSourceId);
  }
  
  function fetchDataSources(callback) {
    gadgets.io.makeRequest(GADGET.adminBaseUrl + "/dataSources", function (response) {
      callback(response.data);
    }, GADGET.jsonParams());
  }
  
  function initDataSources(dataSources) {
	  listDataSources(dataSources);
	  dataSourceSelect.trigger("change");
	}
  
  function listDataSources(dataSources) {
    GADGET.listOptions(dataSourceSelect, dataSources, function (dataSource) {
      return {
        label: dataSource.displayName,
        value: dataSource.id,
        selected: dataSource.id === configuration.dataSourceId
      };
    }, {});
  }
	
	function onDataSourceChanged() {
	  configuration.dataSourceId = this.value;
  }

  function fetchProjects(callback) {
	  gadgets.io.makeRequest(GADGET.jiraProjectBaseUrl, function (response) {
		  callback(response.data);
	  }, GADGET.jsonParams());
  };
	
	function listProjects(projects) {
		GADGET.listOptions(projectSelect, projects, function (project) {
			return { value: project.id, label: project.name, selected: project.id === configuration.projectId };
		}, {});
	}
	
	function listIntervals(select) {
	  gadgets.io.makeRequest(GADGET.adminBaseUrl + "/globalSettings", function (response) {
	    var dataCollectionInterval = response.data.dataCollectionInterval;
	    var options = [];
	    if (dataCollectionInterval === "15m") {
	      options.push({ value: "15m", label: "15 Minutes", selected: configuration.interval === "15m" });
        options.push({ value: "1H", label: "1 Hour", selected: configuration.interval === "1H" });
	    }
	    if (dataCollectionInterval === "3H") {
	      options.push({ value: "3H", label: "3 Hours", selected: configuration.interval === "3H" });
	    }
	    options.push({ value: "1D", label: "1 Day", selected: configuration.interval === "1D" });
	    
	    GADGET.listOptions(select, options, function (option) { return option; }, {
	      includeBlank: false
	    });
	  }, GADGET.jsonParams());
	  
	  return select;
	}
	
	function onProjectChanged() {
		configuration.projectId = this.value;
		
		if (configuration.projectId === "") {
			return;
		}
		configuration.filterId = null;
		configuration.issueKeys = null;
		filterSelect.val("");
	}

	function listFilters(responseFilters) {
		filters = responseFilters;
		GADGET.listOptions(filterSelect, filters, function (filter) {
			return { value: filter.id, label: filter.name, selected: filter.id === configuration.filterId };
		}, {});
	}
	
	function onFilterChanged() {
		configuration.filterId = this.value;
		
		if (configuration.filterId === "") {
			return;
		}
		
		configuration.projectId = null;
		projectSelect.val("");

    var filter = filters.filter(function (filter) {
      return filter.id === configuration.filterId;
    })[0];
    configuration.searchUrl = filter.searchUrl;
    fetchFilterData(filter.searchUrl, function (issues) {
			configuration.issueKeys = issues.map(function (issue) {
				return issue.key;
			});
		});
	}
  
  function fetchFilterData(searchUrl, callback) {
    gadgets.io.makeRequest(searchUrl + "&fields=*all", function (response) {
      callback(response.data.issues);
    }, GADGET.jsonParams());
  }

	function initChart() {
	  var chartConstructor = chartConstructorProvider();
    chart = new chartConstructor(document.getElementById("chartDiv"));
		
		if (configuration.isConfigured) {
      fetchChartData();
		}
	}
	
	function fetchChartData() {
	  var url = GADGET.apiBaseUrl + apiEndpoint + "?dataSourceId=" + configuration.dataSourceId + "&nocache=" + new Date().getTime();
	  
	  if (configuration.projectId !== null) {
	    url += "&projectId=" + configuration.projectId;
	  }
	  
	  if (configuration.issueKeys !== null && configuration.issueKeys.length > 0) {
	    url += "&issueIds=" + configuration.issueKeys.join(",");
	  }
	  
	  if (configuration.fromDate) {
	    url += "&fromDate=" + configuration.fromDate; 
	  }
	  
	  if (configuration.toDate) {
	    url += "&toDate=" + configuration.toDate;
	  }
	  
	  if (configuration.interval) {
	    url += "&interval=" + configuration.interval;
	  }
	  
		gadgets.io.makeRequest(url, function (response) {
	    if (response.data && response.data.status === "success" && response.data.dates) {
	      showChart();
		    onChartDataFetchedCallback(chart, response, configuration);
  	  } else {
  	    //showEditor();
  	    showNone();
  	    GADGET.displayNoDataMessage();
  	  }
		}, GADGET.jsonParams());
	}
	
	function updateEnabledFormElements() {
	  
	  function setDisabled(formElements, disabled) {
	    formElements.forEach(function (formElement) {
	      formElement.attr("disabled", disabled);
	    });
	  }
	
	  if (dataSourceSelect.val() !== "") {
	    setDisabled([projectSelect, filterSelect], false);
      setDisabled([intervalSelect, fromDateInput, toDateInput, saveButton], projectSelect.val() === "" && filterSelect.val() === "");
	  } else {
	    setDisabled([projectSelect, filterSelect, intervalSelect, fromDateInput, toDateInput, saveButton], true);
	  }
	}
	
	function form(id) {
	  return '<form id="' + id + '" class="aui hidden"></form>';
	}
	
	function select(name, label) {
	  return '<div class="field-group">' +
        '<label for="' + name + '">' + label + '</label>' +
        '<select id="' + name + '" name="' + name + '" class="select"></select>' +
      '</div>';
	}
	
	function input(name, label) {
	  return '<div class="field-group">' +
      '<label for="' + name + '">' + label + '</label>' +
      '<input id="' + name + '" name="' + name + '" type="text" class="text" />' +
      '</div>';
	}

	function buttons() {
	  return '<div class="buttons-container">' +
      '<div class="buttons">' +
        '<button id="saveButton" type="button" class="aui-button aui-button-primary">Save</button>' +
        '<button id="cancelButton" type="button" class="aui-button aui-button-link">Cancel</button>' +
      '</div>' +
    '</div>';
	}
	
	initEditor();
	
  var prefs = new gadgets.Prefs();
  var configuration = GADGET.loadConfiguration(prefs);
  var chart;
	var cancelButton = AJS.$("#cancelButton").on("click", onCancelClicked);
	var editButton = AJS.$("#editButton").on("click", showEditor);
	var editorDiv = AJS.$("#editorDiv");
	var saveButton = AJS.$("#saveButton").on("click", onSaveClicked);
  var chartDiv = AJS.$("#chartContainer");
  var messagesContainer = AJS.$("#messages");
	var projectSelect = AJS.$("select[name=projectId]").on("change", onProjectChanged);
	var filterSelect = AJS.$("select[name=filterId]").on("change", onFilterChanged);
	var dataSourceSelect = AJS.$("select[name=dataSourceId]").on("change", onDataSourceChanged);
	var fromDateInput = AJS.$("input[name=fromDate]").val(configuration.fromDate);
	var toDateInput = AJS.$("input[name=toDate]").val(configuration.toDate);
	var intervalSelect = listIntervals(AJS.$("select[name=interval]").val(configuration.interval));
	fromDateInput.datePicker({"overrideBrowserDefault": true});
	/*fromDateInput.datePicker("option", "onClose", function () {
    gadgets.window.adjustHeight();
  });
	fromDateInput.datePicker("option", "onSelect", function () {
    gadgets.window.adjustHeight();
  });*/
	AJS.$(toDateInput).datePicker();
	if (onChartDataFetchedCallback === undefined) {
	  onChartDataFetchedCallback = function (chart, response) {
	    var chartOptions = GADGET.chartOptions();
	    chartOptions.series = response.data.series.map(function (series) {
	      return {
	        color: BAR_COLORS[series.id]
	      };
	    });
      chart.draw(GADGET.dateAndSeriesToChartDataConverter(response), chartOptions);
	  };
	}
	AJS.$("select").on("change", updateEnabledFormElements);
	fetchDataSources(initDataSources);
	fetchProjects(listProjects);
	GADGET.fetchFilters(listFilters);
	loadChartLibrary(initChart);
	if (!configuration.isConfigured) {
		showEditor();
	}
	$(document).on("aui-message-close", showEditor);
};

GADGET.chartOptions = function () {
  return {
    isStacked: true,
    legend: {
      position: "bottom"
    },
    hAxis: {
      format: "yyyy-MM-dd",
      gridlines: {
        color: "transparent"
      }
    },
    vAxis: {
      format: "#"
    },
    pointSize: 3,
    focusTarget: "category",
    backgroundColor: "transparent"
  };
};

GADGET.displayNoDataMessage = function () {
  return AJS.messages.error("#messages", {
    title: "No data was found for this chart",
    body: "Try adjusting the date range. Close to modify the chart configuration.",
    closeable: true
  });
}


GADGET.jsonParams = function () {
  var params = {};
  params[gadgets.io.RequestParameters.CONTENT_TYPE] = gadgets.io.ContentType.JSON;
  
  return params;
};

GADGET.postParams = function () {
  var params = this.jsonParams();
  params[gadgets.io.RequestParameters.METHOD] = gadgets.io.MethodType.POST;
  
  return params;
};

/**
 *  select: the <select> to be populated
 *  optionsData: array of arbitrary objects
 *  optionMapper: mapper that takes an object and returns an object with value and label properties. Can optionally add a selected boolean property.
 *  config: object with the following properties:
 *    - includeBlank: if true, add an empty option at the beginning. Defaults to true.
 *    - triggerChange: if true, trigger change event on select after populating it. Defaults to false.
 */
GADGET.listOptions = function (select, optionsData, optionMapper, config) {
  var _config = AJS.$.extend({ includeBlank: true, triggerChange: false }, config);
  
  var optionsHtml = optionsData.map(function (optionData) {
    var data = optionMapper(optionData);
    return '<option value="' + data.value + '" ' + (data.selected === true ? 'selected' : '') + '>' + data.label + '</option>';
  });
  if (_config.includeBlank) {
    optionsHtml.unshift("<option></option>");
  }
  
  select.html(optionsHtml.join(""))
    .removeClass("hidden");
  if (_config.triggerChange) {
    select.trigger("change");
  }
};

GADGET.loadChartLibrary = function (callback) {
  google.load("visualization", "1.0", {"packages":["corechart"]});
  google.setOnLoadCallback(callback);
};

/**
 * @param callback function that takes response's JSON
 */
GADGET.fetchFilters = function (callback) {
  gadgets.io.makeRequest(GADGET.jiraBaseUrl + "/filter/favourite", function (response) {
    callback(response.data);
  }, GADGET.jsonParams());
};

/**
 * @param select the <select> to be populated
 * @param configuration
 */
GADGET.enableDataSources = function (select, configuration) {
  gadgets.io.makeRequest(GADGET.adminBaseUrl + "/dataSources", function (response) {
    GADGET.listOptions(select, response.data, function (dataSource) {
      return {
        label: dataSource.displayName,
        value: dataSource.id,
        selected: dataSource.id === configuration.dataSourceId
      };
    }, {});
    select.on("change", function () {
      configuration.dataSourceId = select.val();
    });
  }, GADGET.jsonParams());
};

GADGET.loadConfiguration = function (prefs) {
  return {
    dataSourceId: prefs.getString("dataSourceId"),
    projectId: prefs.getString("projectId") || null,
    filterId: prefs.getString("filterId") || null,
    searchUrl: prefs.getString("searchUrl"),
    issueKeys: prefs.getArray("issueKeys"),
    fromDate: prefs.getString("fromDate") || null,
    toDate: prefs.getString("toDate") || null,
    interval: prefs.getString("interval") || null,
    isConfigured: prefs.getBool("isConfigured")
  };
};

GADGET.saveConfiguration = function (prefs, configuration) {
  prefs.set("projectId", configuration.projectId);
  prefs.set("filterId", configuration.filterId);
  prefs.set("searchUrl", configuration.searchUrl);
  prefs.set("dataSourceId", configuration.dataSourceId);
  prefs.set("issueKeys", configuration.issueKeys);
  prefs.set("fromDate", configuration.fromDate);
  prefs.set("toDate", configuration.toDate);
  prefs.set("interval", configuration.interval);
  prefs.set("isConfigured", true);
};

GADGET.dateAndSeriesToChartDataConverter = function (rawData) {
	var headers = ["Date"];
	rawData.data.series.forEach(function (series) {
		headers.push(series.id);
	});
	
	var chartData = [headers];
	rawData.data.dates.forEach(function (date, index) {
		var row = [moment(date, rawData.data.dateFormat).toDate()];
		rawData.data.series.forEach(function (series) {
			row.push(series.data[index]);
		});
		chartData.push(row);
	});
	
	return google.visualization.arrayToDataTable(chartData);
};
