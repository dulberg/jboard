(function () {
  function init() {
    GADGET.init("/testPlanProgress", function () {
      return google.visualization.AreaChart;
    });
  }
  
  gadgets.util.registerOnLoadHandler(init);
})();
