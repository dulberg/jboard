(function () {
	
	function init() {
	  GADGET.init("/testCaseReadiness", function () {
	    return google.visualization.AreaChart;
	  }, function (chart, projectData, configuration) {
	    var chartOptions = GADGET.chartOptions();
	    projectData.data.dateFormat = configuration.interval === "1D" ? "YYYY-MM-DD" : "YYYY-MM-DD HH:mm";
	    var hAxis = {
        format: configuration.interval === "1D" ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm",
        showTextEvery: 4,
        gridlines: {
          color: "transparent"
        }
	    };
	    AJS.$.extend(chartOptions, {
	      hAxis: hAxis
	    });


	    var dashboard = new google.visualization.Dashboard(document.getElementById("chartDiv"));
	    var dates = projectData.data.dates;
	    var rangeStart = moment(dates.length <= 4 ? dates[0] : dates[dates.length - 5], projectData.data.dateFormat).toDate();
      var state = {
        range: {
          start: rangeStart,
          end: moment(dates[dates.length - 1], projectData.data.dateFormat).toDate()
        }
      };
	    var chartRangeFilter = new google.visualization.ControlWrapper({
        "controlType": "ChartRangeFilter",
        "containerId": "chartRangeFilterDiv",
        "options": {
          "filterColumnLabel": "Date",
          "ui": {
            "labelStacking": "vertical",
            "chartType": "AreaChart",
            "snapToData": false,
            "chartOptions": {
              "height": 50,
              isStacked: true,
              backgroundColor: "transparent"
            }
          }
        },
        "state": state
      });
      var chartWrapper =  new google.visualization.ChartWrapper({
        "chartType": "AreaChart",
        "containerId": "chartDiv",
        "options": chartOptions
      });
	    
	    dashboard.bind(chartRangeFilter, chartWrapper);
      dashboard.draw(GADGET.dateAndSeriesToChartDataConverter(projectData), chartOptions);
	  });
	}
	
	gadgets.util.registerOnLoadHandler(init);
})();