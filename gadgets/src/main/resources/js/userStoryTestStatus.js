(function() {
  var configuration = {};
  var chart, prefs, issues, filters, dataTable, chartData;
  var projectSelect, filterSelect, sprintSelect, issuesSelect, dataSourceSelect;
  var saveButton, editButton, editorDiv, chartOverlay;
  var barColors = {
    passed: "#109618",
    failed: "#dc3912",
    blocked: "#ff9900",
    neverRun: "#bbbbbb"
  };
  
  function init() {
    initConfiguration();
    initEditor();
    GADGET.loadChartLibrary(initChart);
  }
  
  function initConfiguration() {
    prefs = new gadgets.Prefs();
    configuration = GADGET.loadConfiguration(prefs);
    configuration.sprintId = prefs.getString("sprintId");
    configuration.issueIds = prefs.getArray("issueIds");
  }
  
  function initEditor() {
    var app = AJS.$("#app");
    var messages = AJS.$('<div id="messages"></div>');

    app.prepend(messages);
    
    if (document.getElementById("chartContainer") === null) {
      app.append('<div id="chartContainer">' +
        '<div id="chartDiv"></div>' +
          '<div id="chartOverlay">' +
            '<button id="editButton" type="button" class="aui-button aui-button-compact">Edit</button>' +
          '</div>' +
        '</div>');
    }

    editorDiv = AJS.$("#editorDiv");
    editButton = AJS.$("#editButton").on("click", toggleEditor);
    chartOverlay = AJS.$("#chartOverlay");
    dataSourceSelect = AJS.$("select[name=dataSourceId]");
    projectSelect = AJS.$("select[name=projectId]").on("change", onProjectChanged);
    filterSelect = AJS.$("select[name=filterId]").on("change", onFilterChanged);
    sprintSelect = AJS.$("select[name=sprintId]").on("change", onSprintChanged);
    issuesSelect = AJS.$("select[name=issueIds]").on("change", onIssuesChanged);
    saveButton = AJS.$("#saveButton").on("click", onSaveClicked);
    AJS.$("#cancelButton").on("click", toggleEditor);
    AJS.$("select").on("change", updateEnabledFormElements);

    listProjects();
    GADGET.fetchFilters(listFilters);
    GADGET.enableDataSources(dataSourceSelect, configuration);
    updateEnabledFormElements();
    if (!configuration.isConfigured) {
      toggleEditor();
    }
  }
  
  function listProjects() {
    gadgets.io.makeRequest(GADGET.agileBaseUrl + "/rapidviews/list", function (response) {
      listOptions(projectSelect, response.data.views, configuration.projectId);
    }, GADGET.jsonParams());
  }
  
  function listFilters(filtersResponse) {
    filters = filtersResponse;
    listOptions(filterSelect, filters, configuration.filterId);
  }
  
  function onProjectChanged() {
    if (this.value === "") {
      configuration.projectId = null;
    
      return;
    }
    
    configuration.projectId = this.value;
    configuration.sprintId = null;
    sprintSelect.val("");
    configuration.issuesId = null;
    issuesSelect.val([]);
    configuration.filterId = null;
    filterSelect.val("");
    
    listProjectSprints();
  }
  
  function listProjectSprints() {
    gadgets.io.makeRequest(GADGET.agileBaseUrl + "/sprintquery/" + configuration.projectId + "?includeHistoricSprints=true&includeFutureSprints=true", function (response) {
      listOptions(sprintSelect, response.data.sprints, configuration.sprintId);
    }, GADGET.jsonParams());
  }
  
  function loadConfiguredSprintIssues(configuredSprintId, configuredIssueIds) {
    var selectedPredicate = function (issue) {
      return configuredIssueIds.indexOf(issue.id) > -1;
    };
    var url = GADGET.jiraBaseUrl + "/search?fields=*all&maxResults=50&jql=sprint%3D" + configuredSprintId;
    var listIssues = function (response) {
      issues = response.data.issues;
      GADGET.listOptions(issuesSelect, issues, function (issue) {
        return { value: issue.id, label: "[" + issue.key + "] " + issue.fields.summary, selected: selectedPredicate(issue) };
      }, { includeBlank: false, triggerChange: true });
    };
    
    gadgets.io.makeRequest(url, function (response) {
      listIssues(response);
      updateChart();
    }, GADGET.jsonParams());
  }
  
  function onSprintChanged() {
    configuration.sprintId = this.value;
    loadSprintIssues();
  }
  
  function loadSprintIssues() {
    var url = GADGET.jiraBaseUrl + "/search?fields=*all&maxResults=50&jql=sprint%3D" + configuration.sprintId;
    var listIssues = function (response) {
      issues = response.data.issues;
      GADGET.listOptions(issuesSelect, issues, function (issue) {
        return { value: issue.key, label: "[" + issue.key + "] " + issue.fields.summary, selected: true };
      }, { includeBlank: false, triggerChange: true });
    };
    
    gadgets.io.makeRequest(url, listIssues, GADGET.jsonParams());
  }
  
  function onFilterChanged() {
    if (this.value === "") {
      configuration.filterId = null;
      return;
    }
    
    configuration.filterId = this.value;
    configuration.projectId = null;
    projectSelect.val(null);
    configuration.sprintId = null;
    sprintSelect.val(null);
    configuration.issuesId = null;
    issuesSelect.val([]);
    var filter = filters.filter(function (filter) {
      return filter.id === configuration.filterId;
    })[0];
    configuration.searchUrl = filter.searchUrl;
    fetchFilterData(filter.searchUrl);
  }
  
  function fetchFilterData(searchUrl, callback) {
    var listIssues = function (response) {
      issues = response.data.issues;
      GADGET.listOptions(issuesSelect, issues, function (issue) {
        return { value: issue.key, label: "[" + issue.key + "] " + issue.fields.summary, selected: true };
      }, { includeBlank: false, triggerChange: true });
    };
    
    gadgets.io.makeRequest(searchUrl + "&fields=*all", function (response) {
      listIssues(response);
      if (callback) {
        callback();
      }
    }, GADGET.jsonParams());
  }
  
  function onIssuesChanged() {
    configuration.issueIds = issuesSelect.val();
  }
  
  function onSaveClicked(event) {
    event.preventDefault();
		toggleEditor();
    saveConfiguration();
    updateChart();
  }
  
  function updateChart() {
    // "/userStoryTestStatus?issueIds=" + configuration.issueIds.join(",")
    gadgets.io.makeRequest(GADGET.apiBaseUrl + "/userStoryTestStatus?dataSourceId=" + configuration.dataSourceId
      + "&issueIds=" + configuration.issueIds.join(",")
      + "&nocache=" + new Date().getTime(), function (response) {
        chartData = response.data
        drawChart(chartData);
      }, GADGET.jsonParams());
  }
  
  function saveConfiguration() {
    GADGET.saveConfiguration(prefs, configuration);
    prefs.set("sprintId", configuration.sprintId);
    prefs.setArray("issueIds", configuration.issueIds);
  }

  function initChart() {
    chart = new google.visualization.BarChart(document.getElementById("chartDiv"));
    
    google.visualization.events.addListener(chart, "select", function () {
      var selection = chart.getSelection()[0];
      if (selection) {
        window.top.location.href = getIssueUrl(chartData.data[selection.row].key)
      }
    });

    
    if (configuration.isConfigured) {
      if (configuration.projectId) {
        listProjectSprints();
        loadSprintIssues();
        loadConfiguredSprintIssues(configuration.sprintId, configuration.issueIds);
      } else if (configuration.filterId) {
        fetchFilterData(configuration.searchUrl, updateChart);
      }
    }
  }

  function drawChart(rawData) {
    dataTable = getDataTable(rawData);
    var chartOptions = $.extend({}, GADGET.chartOptions(), {
      legend : {
        position : "top"
      },
      hAxis: {
        format: "#",
        gridlines: {
          count: getMaxValue(rawData)
        }
      },
      chartArea: {
        width: "60%"
      }
    });
    delete chartOptions.focusTarget;
    
    chartOptions.series = rawData.series.map(function (series) {
      return {
        color: barColors[series.id]
      };
    });
    
    chart.draw(dataTable, chartOptions);
    gadgets.window.adjustHeight();
  }

  function getDataTable(rawData) {
    var chartData = [];
    var labels = rawData.series.map(function (series) {
      return series.name;
    });
    labels.unshift("User Story");
    chartData.push(labels);

    rawData.data.forEach(function (story) {
      var row = [];
      row.push(story.name);
      rawData.series.forEach(function (series) {
        row.push(story[series.id]);
      });
      chartData.push(row);
    });
    
    return google.visualization.arrayToDataTable(chartData);
  }

  function getMaxValue(rawData) {
    return rawData.data.map(function (story) {
      var value = 0;
      rawData.series.forEach(function (series) {
        value += story[series.id];
      });

      return value;
    }).sort().pop();
  }

  function listOptions(select, optionsData, selectedId) {
    GADGET.listOptions(select, optionsData, function (item) {
        return { value: item.id, label: item.name, selected: item.id === selectedId };
      }, {});
  }
	
	function toggleEditor() {
		editButton.toggleClass("hidden");
		chartOverlay.toggleClass("hidden");
		editorDiv.toggleClass("hidden");
		AJS.$("#chartDiv").toggleClass("hidden");
    
    gadgets.window.adjustHeight();
	}
	
	function getIssueUrl(key) {
	  return GADGET.baseUrl + "/browse/" + key;
	}
	
	function isSet(element) {
	  return element.val() !== null && element.val() !== "";
	}
	
	function updateEnabledFormElements() {
	  if (!isSet(dataSourceSelect)) {
	    disable([projectSelect, filterSelect, sprintSelect, issuesSelect, saveButton]);
	    
	    return;
	  }
	  
	  enable([projectSelect, filterSelect]);
	  
	  if (!isSet(projectSelect) && !isSet(filterSelect)) {
	    disable([sprintSelect, issuesSelect, saveButton]);
	    
	    return;
	  }
	  
	  if (!isSet(projectSelect)) {
	    disable([sprintSelect]);
	    enable([issuesSelect]);
	  } else {
	    enable([sprintSelect]);
	    
	    if (!isSet(sprintSelect)) {
	      disable([issuesSelect]);
	    } else {
	      enable([issuesSelect]);
	    }
	  }
	  
	  if (issuesSelect.val() === null) {
	    disable([saveButton]);
	  } else {
	    enable([saveButton]);
	  }
	}
	
	function enable(formElements) {
	  formElements.forEach(function (formElement) {
	    formElement.attr("disabled", false);
	  });
	}
	
	function disable(formElements) {
	  formElements.forEach(function (formElement) {
	    formElement.attr("disabled", true);
	  });
	}
	
  // Returns a random integer between min (included) and max (excluded)
  // Using Math.round() will give you a non-uniform distribution!
  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  gadgets.util.registerOnLoadHandler(init);
})();