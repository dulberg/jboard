AJS.toInit(function() {
    var baseUrl = AJS.$("meta[name='application-base-url']").attr("content") + "/rest/jboard/1.0";

    function populateForm() {
        AJS.$.ajax({
            url: baseUrl + "/admin",
            dataType: "json",
            success: function(config) {
                AJS.$("#name").attr("value", config.name);
                AJS.$("#time").attr("value", config.time);
            }
        });
    }
    function updateConfig() {
        AJS.$.ajax({
            url: baseUrl + "/admin",
            type: "PUT",
            contentType: "application/json",
            data: '{ "name": "' + AJS.$("#name").attr("value") + '", "time": ' +  AJS.$("#time").attr("value") + ' }',
            processData: false
        });
    }
    // populateForm();

//    AJS.$("#admin").submit(function(e) {
//        e.preventDefault();
//        updateConfig();
//    });
    
    AJS.$("button[data-test-connection]").click(function (event) {
      event.preventDefault();
      var button = AJS.$(this);
      AJS.$.getJSON(baseUrl + "/admin/dataSources/" + button.data("test-connection") + "/test", function (response) {
        if (response.result === "success") {
          button.siblings("div[data-test-connection]").html("<span class='aui-lozenge aui-lozenge-success'>OK</span>");
        } else {
          button.siblings("div[data-test-connection]").html("<span class='aui-lozenge aui-lozenge-error'>FAILURE</span><p class='error'>" + response.message + "</p>");
        }
      });
    });
    
});
