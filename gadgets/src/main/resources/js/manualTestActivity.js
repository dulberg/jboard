(function () {
  var chartOptions = GADGET.chartOptions();
  chartOptions.legend = {
    position: "none"
  };

	function init() {
	  GADGET.init("/manualTestActivity", function () {
	    return google.visualization.LineChart;
	  }, updateChart);
	}
	
	function updateChart(chart, rawData) {
		chart.draw(GADGET.dateAndSeriesToChartDataConverter(rawData), chartOptions);
	}
	
	gadgets.util.registerOnLoadHandler(init);
})();
