(function () {
	function init() {
	  GADGET.init("/testFailureAnalysis", function () {
	    return google.visualization.AreaChart;
    });
	}
	
	gadgets.util.registerOnLoadHandler(init);
})();
