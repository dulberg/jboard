package jboard.timeseries;

import com.atlassian.sal.api.scheduling.PluginJob;

public interface TestCaseReadinessSchedulerComponent {

	void reschedule(long interval);
	void updateJob();
}
