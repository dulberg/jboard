package jboard.timeseries;

import java.util.Date;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("TS_TCR")
public interface TestCaseReadinessTimeSeriesItem extends Entity {

	String getAlmId();
	void setAlmId(String almId);
	String getStatus();
	void setStatus(String status);
	String getExecStatus();
	void setExecStatus(String execStatus);
	Date getLastModifiedDate();
	void setLastModifiedDate(Date lastModifiedDate);
	String getAlmDomain();
	void setAlmDomain(String domain);
	String getAlmProject();
	void setAlmProject(String project);
	Date getItemCreationDate();
	void setItemCreationDate(Date itemCreationDate);
}
