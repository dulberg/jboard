package jboard.timeseries;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.bb.jboard.admin.AdminServlet;
import com.bb.jboard.admin.DataSource;

/**
 * To work around timing issues with ActiveObjects, much of the code here is taken from https://bitbucket.org/cfuller/atlassian-scheduler-jira-example/src/90a07c5af0fbd6272c034100923a50f892ce7d07/src/main/java/com/atlassian/jira/plugins/example/scheduler/impl/AwesomeLauncher.java?at=master&fileviewer=file-view-default
 */
public class TestCaseReadinessScheduler implements TestCaseReadinessSchedulerComponent, LifecycleAware, InitializingBean, DisposableBean {
	
	private static final String PLUGIN_KEY = "com.bb.jboard.gadgets";
	private final PluginScheduler pluginScheduler;
	private final PluginSettingsFactory pluginSettingsFactory;
	private final Set<LifecycleEvent> lifecycleEvents = EnumSet.noneOf(LifecycleEvent.class);
	private final ActiveObjects activeObjects;
	private final EventPublisher eventPublisher;

	public TestCaseReadinessScheduler(PluginScheduler pluginScheduler, PluginSettingsFactory pluginSettingsFactory, EventPublisher eventPublisher, ActiveObjects activeObjects) {
		this.pluginScheduler = pluginScheduler;
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.eventPublisher = eventPublisher;
		this.activeObjects = activeObjects;
	}
	
	@Override
	public void updateJob() {
		PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
		long refreshInterval = getRefreshInterval((String) pluginSettings.get(AdminServlet.REFRESH_INTERVAL_SETTINGS_KEY));
		pluginScheduler.scheduleJob(TestCaseReadinessPluginJob.class.getName() + ":job", TestCaseReadinessPluginJob.class, createJobData(), new Date(), refreshInterval);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("**************************************\nTestCaseReadinessScheduler.afterPropertiesSet()\n*********************************");
        registerListener();
	}

	@Override
	public void onStart() {
		System.out.println("**************************************\nTestCaseReadinessScheduler.onStart(): Start Called\n*********************************");
		registerJobs(new DateTime().plusMinutes(2).toDate());
	}

    /**
     * This is received from the plugin system after the plugin is fully initialized.  It is not safe to use
     * Active Objects before this event is received.
     */
    @EventListener
	public void onPluginEnabled(PluginEnabledEvent event) {
		String eventPluginKey = event.getPlugin().getKey();
		if (eventPluginKey.contains("active")) {
			System.out.println("\n\n***********************************\nTestCaseReadinessScheduler.onPluginEnabled(): " + event.getPlugin().getKey() + "\n\n\n*******************************************************\n");
		}
		if (PLUGIN_KEY.equals(eventPluginKey)) {
			System.out.println("\n\n***********************************\nTestCaseReadinessScheduler.onPluginEnabled() THIS: " + event.getPlugin().getKey() + "\n\n\n*******************************************************\n");
		}
	}

	@Override
	public void destroy() throws Exception {
        unregisterListener();
        unregisterJobs();
	}

	@Override
	public void reschedule(long interval) {
	}

	private void registerListener() {
		eventPublisher.register(this);
	}

	private void unregisterListener() {
		eventPublisher.unregister(this);
	}

	private void unregisterJobs() {
		pluginScheduler.unscheduleJob(TestCaseReadinessPluginJob.class.getName() + ":job");
	}

	private void registerJobs(Date startDate) {
		System.out.println("\n\n*****************************************\n\nTestCaseReadinessScheduler.registerJobs()\n*****************************************");
		PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
		long refreshInterval = getRefreshInterval((String) pluginSettings.get(AdminServlet.REFRESH_INTERVAL_SETTINGS_KEY));
		
		System.out.println(new Date() + " // TestCaseReadinessScheduler.registerJobs(): Start Date: " + startDate);
		
		pluginScheduler.scheduleJob(TestCaseReadinessPluginJob.class.getName() + ":job", TestCaseReadinessPluginJob.class, createJobData(), startDate, refreshInterval);
		System.out.println("TestCaseReadinessScheduler.reschedule(): End");
	}

	private Map<String, Object> createJobData() {
		PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        List<DataSource> dataSources = new ArrayList<DataSource>();
        String dataSourceKeysString = (String) pluginSettings.get(AdminServlet.DATA_SOURCE_KEYS);
        
        if (dataSourceKeysString != null) {
        	try {
        		JSONArray dataSourceKeys =  new JSONArray(dataSourceKeysString);
        		
        		for (int i = 0; i < dataSourceKeys.length(); i++) {
        			String id = dataSourceKeys.getString(i);
        			JSONObject dataSource = new JSONObject((String) pluginSettings.get(AdminServlet.DATA_SOURCE_KEYS + ":" + id));
        			dataSources.add(DataSource.from(dataSource));
        		}
        	} catch (JSONException e) {
        		throw new RuntimeException(e);
        	}
        }
		
		Map<String, Object> jobDataMap = new HashMap<String, Object>();
		jobDataMap.put(ActiveObjects.class.getName(), activeObjects);
		jobDataMap.put(DataSource.class.getName(), dataSources);
		return jobDataMap;
	}
	
	private long getRefreshInterval(String s) {
		if ("15m".equals(s)) {
			return 15 * 60 * 1000;
		} else if ("3H".equals(s)) {
			return 3 * 60 * 60 * 1000;
		} else if ("1D".equals(s)) {
			return 24 * 60 * 60 * 1000;
		} else {
			return 15 * 60 * 1000;
		}
	}

	/**
	 * Used to keep track of everything that needs to happen before we are sure
	 * that it is safe to talk to all of the components we need to use,
	 * particularly the {@code SchedulerService} and Active Objects. We will not
	 * try to initialize until all of them have happened.
	 */
	static enum LifecycleEvent {
		AFTER_PROPERTIES_SET, PLUGIN_ENABLED, LIFECYCLE_AWARE_ON_START
	}
}
