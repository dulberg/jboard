package jboard.timeseries;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.time.DateUtils;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.bb.jboard.admin.DataSource;
import com.bb.jboard.hpalm.HpAlmService;
import com.bb.jboard.hpalm.Project;
import com.bb.jboard.hpalm.Test;
import com.bb.jboard.hpalm.v11_52.HpAlmServiceV11_52;

public class TestCaseReadinessPluginJob implements PluginJob {
	
	private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
	

	@Override
	public void execute(Map<String, Object> jobDataMap) {
		System.out.println("TestCaseReadinessPluginJob.execute(): Start job");
		@SuppressWarnings("unchecked")
		List<DataSource> dataSources = (List<DataSource>) jobDataMap.get(DataSource.class.getName());
		final ActiveObjects activeObjects = (ActiveObjects) jobDataMap.get(ActiveObjects.class.getName());
		
		if (INITIALIZED.compareAndSet(false, true)) {
			activeObjects.flushAll();
		}
		
		System.out.println("\n\n**********************************************************\nTestCaseReadinessPluginJob.execute():");
		
		final Date jobExecutionDate = new Date();

		for (final DataSource dataSource : dataSources) {
			System.out.println("\tDatasource: " + dataSource.getDisplayName());
			HpAlmService almService = new HpAlmServiceV11_52(dataSource);
			almService.authenticate();

			try {
				Project project = almService.getProject(dataSource);
				List<Test> tests = almService.getTests(null, null, "status[\"Design\"or\"Ready\"]", project);
				
				for (final Test test : tests) {
					activeObjects.executeInTransaction(new TransactionCallback<TestCaseReadinessTimeSeriesItem>() {
						@Override
						public TestCaseReadinessTimeSeriesItem doInTransaction() {
							TestCaseReadinessTimeSeriesItem item = activeObjects.create(TestCaseReadinessTimeSeriesItem.class);
							item.setAlmId(test.getId());
							item.setAlmDomain(dataSource.getDomain());
							item.setAlmProject(dataSource.getProject());
							item.setExecStatus(test.getExecStatus().getXmlValue());
							item.setStatus(test.getStatus());
							item.setLastModifiedDate(test.getLastModifiedDate());
							item.setItemCreationDate(jobExecutionDate);
							item.save();
							
							return item;
						}
					});
				}
			} finally {
				almService.logout();
				System.out.println("TestCaseReadinessPluginJob.execute(): End job");
			}
		}
	}
}
