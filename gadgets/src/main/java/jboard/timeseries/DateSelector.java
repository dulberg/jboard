package jboard.timeseries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.time.FastDateFormat;
import org.joda.time.Duration;
import org.joda.time.Interval;

public class DateSelector {
	
	private static final DateSelector DAILY = new DateSelector(new Duration(24 * 60 * 60 * 1000), FastDateFormat.getInstance("yyyy-MM-dd"));
	private static final DateSelector THREE_HOURLY = new DateSelector(new Duration(3 * 60 * 60 * 1000), FastDateFormat.getInstance("yyyy-MM-dd HH:mm"));
	private static final DateSelector HOURLY = new DateSelector(new Duration(60 * 60 * 1000), FastDateFormat.getInstance("yyyy-MM-dd HH:mm"));
	private static final DateSelector QUARTER_HOURLY = new DateSelector(new Duration(15 * 60 * 1000), FastDateFormat.getInstance("yyyy-MM-dd HH:mm"));
	private static final DateSelector MINUTELY = new DateSelector(new Duration(60 * 1000), FastDateFormat.getInstance("yyyy-MM-dd HH:mm"));
	
	private final FastDateFormat format;
	private final Duration minimumDuration;
	
	public static DateSelector create(String interval) {
		if ("1D".equals(interval)) {
			return DAILY;
		} else if ("3H".equals(interval)) {
			return THREE_HOURLY;
		} else if ("1H".equals(interval)) {
			return HOURLY;
		} else if ("15m".equals(interval)) {
			return QUARTER_HOURLY;
		} else if ("1m".equals(interval)) {
			return MINUTELY;
		} else {
			throw new IllegalArgumentException("Unknown DateSelector interval: " + interval);
		}
	}
	
	public List<Date> filter(Collection<Date> dates) {
		ArrayList<Date> filteredDates = new ArrayList<Date>(dates);
		Iterator<Date> datesIterator = filteredDates.iterator();
		Date current = null;
		while (datesIterator.hasNext()) {
			Date candidate = datesIterator.next();
			if (current == null) {
				current = candidate;
			} else if (isIntervalReached(current, candidate)) {
				current = candidate;
			} else {
				datesIterator.remove();
			}
		}
		
		return filteredDates;
	}
	
	private boolean isIntervalReached(Date current, Date candidate) {
		Duration duration = new Interval(candidate.getTime(), current.getTime()).toDuration();
		return duration.isEqual(minimumDuration) || duration.isLongerThan(minimumDuration);
	}
	
	public FastDateFormat getDateFormat() {
		return format;
	}
	
	private DateSelector(Duration duration, FastDateFormat format) {
		this.minimumDuration = duration;
		this.format = format;
	}
}
