package jira;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.sal.api.user.UserManager;

public class JboardUserService {
	
	private final UserUtil userUtil;
	private final UserManager userManager;
	
	public JboardUserService(UserManager userManager, UserUtil userUtil) {
		this.userManager = userManager;
		this.userUtil = userUtil;
	}
	
	public ApplicationUser getLoggedInUser() {
		return userUtil.getUserByName(userManager.getRemoteUser().getUsername());
	}
}
