package jira;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;

interface JboardSearchServiceDelegate {

	SearchResults search(ApplicationUser applicationUser, String queryString) throws SearchException;
}
