package jira;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.search.SearchService.ParseResult;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;

class JboardSearchServiceDelegateV7 implements JboardSearchServiceDelegate {
	
	private final SearchService searchService;
	
	public JboardSearchServiceDelegateV7(SearchService searchService) {
		this.searchService = searchService;
	}
	
	@Override
	public SearchResults search(ApplicationUser applicationUser, String queryString) throws SearchException {
		try {
			Query query = ((ParseResult) SearchService.class.getMethod("parseQuery", ApplicationUser.class, String.class).invoke(searchService, applicationUser, queryString)).getQuery();
			
			return (SearchResults) SearchService.class.getMethod("search", ApplicationUser.class, Query.class, PagerFilter.class).invoke(searchService, applicationUser, query, new PagerFilter<Object>(-1));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
