package jira;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;

public class JboardSearchService {
	
	private final JboardSearchServiceDelegate delegate;
	
	public JboardSearchService(SearchService searchService) {
		boolean useV7 = true;
		try {
			SearchService.class.getMethod("parseQuery", ApplicationUser.class, String.class);
			System.out.println("JboardSearchService.JboardSearchService(): use V7");
		} catch (NoSuchMethodException e) {
			useV7 = false;
			System.out.println("JboardSearchService.JboardSearchService(): use V6");
		}
		
		if (useV7) {
			this.delegate = new JboardSearchServiceDelegateV7(searchService);
		} else {
			this.delegate = new JboardSearchServiceDelegateV6(searchService);
		}
	}
	
	public SearchResults search(ApplicationUser applicationUser, String queryString) throws SearchException {
		return delegate.search(applicationUser, queryString);
	}

}
