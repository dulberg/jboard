package jira;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;

class JboardSearchServiceDelegateV6 implements JboardSearchServiceDelegate {
	
	private final SearchService searchService;
	
	public JboardSearchServiceDelegateV6(SearchService searchService) {
		this.searchService = searchService;
	}
	
	@Override
	public SearchResults search(ApplicationUser applicationUser, String queryString) throws SearchException {
		User user = applicationUser.getDirectoryUser();
		Query query = searchService.parseQuery(user, queryString).getQuery();
		
		return searchService.search(user, query, new PagerFilter<Object>(-1));
	}

}
