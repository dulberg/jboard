package com.bb.jboard.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jboard.org.apache.commons.lang3.time.FastDateParser;
import jboard.timeseries.DateSelector;
import jboard.timeseries.TestCaseReadinessTimeSeriesItem;
import jira.JboardSearchService;
import jira.JboardUserService;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.bb.jboard.admin.AdminServlet;
import com.bb.jboard.admin.DataSource;
import com.bb.jboard.hpalm.HpAlmService;
import com.bb.jboard.hpalm.Project;
import com.bb.jboard.hpalm.Requirement;
import com.bb.jboard.hpalm.Run;
import com.bb.jboard.hpalm.Test;
import com.bb.jboard.hpalm.TestInstance;
import com.bb.jboard.hpalm.v11_52.HpAlmServiceV11_52;

/**
 * Endpoints used by gadgets to display their charts.
 */
@Path("/api")
public class JboardResource {

	private final UserSettingsService userSettingsService;
	private final UserManager userManager;
	private final PluginSettingsFactory pluginSettingsFactory;
	private final JboardSearchService searchService;
	private final ActiveObjects activeObjects;
	private final JboardUserService jboardUserService;

	public JboardResource(PluginSettingsFactory pluginSettingsFactory, UserSettingsService userSettingsService, UserManager userManager, UserUtil userUtil, SearchService searchService, ActiveObjects activeObjects) {
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.userManager = userManager;
		this.userSettingsService = userSettingsService;
		this.searchService = new JboardSearchService(searchService);
		this.activeObjects = activeObjects;
		this.jboardUserService = new JboardUserService(userManager, userUtil);
	}

  /**
   * Endpoint for the User Story Test Status chart. Response data is modeled by UserStoryTestStatusResponse.
   */
	@GET
    @Path("/userStoryTestStatus")
    @Produces(APPLICATION_JSON)
    public Response getUserStoryTestStatus(@QueryParam("dataSourceId") String dataSourceId, @QueryParam("issueIds") @DefaultValue("") String issueIdString) throws SearchException {
		Set<String> issueIds = splitIssueKeys(issueIdString);
		DataSource dataSource = getDataSource(dataSourceId);
		HpAlmService almService = new HpAlmServiceV11_52(dataSource);
		almService.authenticate();
		
		UserStoryTestStatusResponse entity = new UserStoryTestStatusResponse();
    	try {
    		Project project = almService.getProject(dataSource);
    		List<Requirement> requirements = almService.getRequirements(project);
    		
    		ApplicationUser user = jboardUserService.getLoggedInUser();
    		for (Requirement requirement : requirements) {
    			if (!issueIds.isEmpty() && !issueIds.contains(requirement.getJiraIssueKey())) {
    				continue;
    			}
    			
    			SearchResults searchResults = searchService.search(user, "issue=" + requirement.getJiraIssueKey());
    			
				Issue issue = searchResults.getIssues().get(0);
				UserStoryTestStatusResponse.Data data = new UserStoryTestStatusResponse.Data(issue.getSummary(), issue.getKey());
				for (Test test : requirement.getTests()) {
					data.add(test);
				}
				entity.add(data);
			}
    	} finally {
    		almService.logout();
    	}
    	
    	return Response.ok(entity).build();
    }
	
	/**
	 * Endpoint for Manual Test Activity chart. JSON response is modeled by SeriesResponse.
	 */
	@GET
	@Path("/manualTestActivity")
	@Produces(APPLICATION_JSON)
	public Response getManualTestActivity(@QueryParam("dataSourceId") String dataSourceId, @QueryParam("issueIds") @DefaultValue("") String issueIdString, @QueryParam("projectId") @DefaultValue("") String projectId, @QueryParam("fromDate") @DefaultValue("") String fromDate, @QueryParam("toDate") @DefaultValue("") String toDate) throws SearchException {
		Set<String> issueKeys = splitIssueKeys(issueIdString);
		DataSource dataSource = getDataSource(dataSourceId);
		HpAlmService almService = new HpAlmServiceV11_52(dataSource);
		almService.authenticate();
		SeriesResponse seriesResponse = new SeriesResponse();
		
    	try {
    		Project project = almService.getProject(dataSource);
    		List<Test> tests = almService.getTests(getFrom(fromDate), getTo(toDate), "subtype-id[MANUAL]", project);
    		List<Run> runs = almService.getRuns(getFrom(fromDate), getTo(toDate), "subtype-id[hp.qc.run.MANUAL]", project);
    		
    		if (issueKeys.isEmpty() && !projectId.isEmpty()) {
    			issueKeys = getIssueKeysFromJiraProject(projectId);
    		}

    		if (!issueKeys.isEmpty()) {
    			Set<String> testIds = getTestIds(issueKeys, almService, project);
    			filterTests(tests, testIds);
    			filterRuns(runs, testIds);
    		}
    		
    		for (Run run :runs) {
    			seriesResponse.increment(run.getExecutionDate(), "testAmounts");
    		}
    	} finally {
    		almService.logout();
    	}
		
		return Response.ok(seriesResponse.toJson()).build();
	}
	
	/**
	 * Endpoint for Test Plan Progress chart. JSON response is modeled by SeriesResponse.
	 */
	@GET
	@Path("/testPlanProgress")
	@Produces(APPLICATION_JSON)
	public Response getTestPlanProgress(@QueryParam("dataSourceId") String dataSourceId, @QueryParam("issueIds") @DefaultValue("") String issueKeysString, @QueryParam("projectId") @DefaultValue("") String projectId, @QueryParam("fromDate") @DefaultValue("") String fromDate, @QueryParam("toDate") @DefaultValue("") String toDate) throws SearchException {
		DataSource dataSource = getDataSource(dataSourceId);
		SeriesResponse response = new SeriesResponse();
		HpAlmService almService = new HpAlmServiceV11_52(dataSource);
		almService.authenticate();
		
    	try {
    		Project project = almService.getProject(dataSource);
    		Date from = getFrom(fromDate);
    		Date to = getTo(toDate);
    		List<Run> runs = almService.getRuns(from, to, "", project);
    		for (Run run : runs) {
				System.out.println("JboardResource.getTestPlanProgress(): All Runs ID: " + run.getTestId());
			}
    		almService.getRequirements(project);
    		List<Test> tests = almService.getRecentTests(project, "");
    		Map<Date, Set<String>> testIdsPerExecutionDate = new HashMap<Date, Set<String>>();
    		
    		Set<String> issueKeys = splitIssueKeys(issueKeysString);
    		if (issueKeys.isEmpty() && !projectId.isEmpty()) {
    			issueKeys = getIssueKeysFromJiraProject(projectId);
    		}
    		
			if (!issueKeys.isEmpty()) {
				Set<String> testIds = getTestIds(issueKeys, almService, project);
				filterTests(tests, testIds);
				filterRuns(runs, testIds);
			}
    		
    		for (Run run : runs) {
    			System.out.println("JboardResource.getTestPlanProgress(): Run ID: " + run.getTestId());
				Date executionDate = run.getExecutionDate();
				
				response.increment(executionDate, run.getStatus());
				if (!testIdsPerExecutionDate.containsKey(executionDate)) {
					testIdsPerExecutionDate.put(executionDate, new HashSet<String>());
				}
				testIdsPerExecutionDate.get(executionDate).add(run.getTestId());
			}
    		
    		Set<String> allTestIds = new HashSet<String>();
    		for (Test test : tests) {
    			allTestIds.add(test.getId());
    		}
    		for (Map.Entry<Date, Set<String>> entry : testIdsPerExecutionDate.entrySet()) {
    			for (String testId : allTestIds) {
    				if (!entry.getValue().contains(testId)) {
    					response.increment(entry.getKey(), "No Run");
    				}
    			}
    		}
    	} finally {
    		almService.logout();
    	}
		
		return Response.ok(response.toJson()).build();
	}
	
	/**
	 * Endpoint for Test Failure Analysis chart. JSON response is modeled by SeriesResponse.
	 */
	@GET
	@Path("/testFailureAnalysis")
	@Produces(APPLICATION_JSON)
	public Response getTestFailureAnalysis(@QueryParam("dataSourceId") String dataSourceId, @QueryParam("issueIds") @DefaultValue("") String issueIdString, @QueryParam("fromDate") @DefaultValue("") String fromDate, @QueryParam("toDate") @DefaultValue("") String toDate) {
		Set<String> issueIds = splitIssueKeys(issueIdString);
		DataSource dataSource = getDataSource(dataSourceId);
		SeriesResponse response = new SeriesResponse();
		HpAlmService almService = new HpAlmServiceV11_52(dataSource);
		almService.authenticate();

		try {
			Project project = almService.getProject(dataSource);
			List<TestInstance> testInstances = almService.getTestInstances(getFrom(fromDate), getTo(toDate), project, issueIds);

			for (TestInstance testInstance : testInstances) {
				response.increment(testInstance.getExecDate(), testInstance.getStatus());
			}
		} finally {
			almService.logout();
		}

		return Response.ok(response.toJson()).build();
	}
	
	/**
	 * Endpoint for Test Case Readiness chart. JSON response is modeled by SeriesResponse.
	 */
	@GET
	@Path("/testCaseReadiness")
	@Produces(APPLICATION_JSON)
	public Response getTestCaseReadiness(@QueryParam("dataSourceId") String dataSourceId, @QueryParam("issueIds") @DefaultValue("") String jiraIssueKeysString, @QueryParam("projectId") String jiraProjectKey, @QueryParam("fromDate") @DefaultValue("") String fromDate, @QueryParam("toDate") @DefaultValue("") String toDate, @QueryParam("interval") @DefaultValue("1D") String interval) throws ParseException, Exception {
		DataSource dataSource = getDataSource(dataSourceId);
		DateSelector dateSelector = DateSelector.create(interval);
		
		Date from = !fromDate.isEmpty() ? FastDateParser.ISO_DATE_FORMAT.parse(fromDate) : fourWeeksAgo();
		Date to = !toDate.isEmpty() ? FastDateParser.ISO_DATE_FORMAT.parse(toDate) : new Date();
		
		Set<String> jiraIssueKeys = splitIssueKeys(jiraIssueKeysString);
		if (jiraIssueKeys.isEmpty() && !jiraProjectKey.isEmpty()) {
			jiraIssueKeys = getIssueKeysFromJiraProject(jiraProjectKey);
		}
		
		SeriesResponse response = new SeriesResponse(dateSelector.getDateFormat());
		List<TestCaseReadinessTimeSeriesItem> items = new ArrayList<TestCaseReadinessTimeSeriesItem>(Arrays.asList(activeObjects.find(TestCaseReadinessTimeSeriesItem.class, "ALM_DOMAIN = ? AND ALM_PROJECT = ? AND ITEM_CREATION_DATE >= ? AND ITEM_CREATION_DATE < ? ORDER BY ITEM_CREATION_DATE DESC", dataSource.getDomain(), dataSource.getProject(), from, to)));
		
		if (!jiraIssueKeys.isEmpty()) {
			HpAlmService almService = new HpAlmServiceV11_52(dataSource);
			almService.authenticate();
			Set<String> testIds = getTestIds(jiraIssueKeys, almService, almService.getProject(dataSource));
			Iterator<TestCaseReadinessTimeSeriesItem> itemsIterator = items.iterator();
			while (itemsIterator.hasNext()) {
				if (!testIds.contains(itemsIterator.next().getAlmId())) {
					itemsIterator.remove();
				}
			}
		}
		
		Map<String, List<TestCaseReadinessTimeSeriesItem>> itemsByAlmId = new HashMap<String, List<TestCaseReadinessTimeSeriesItem>>();
		SortedSet<Date> datesReversed = new TreeSet<Date>(new Comparator<Date>() {
			@Override
			public int compare(Date o1, Date o2) {
				return o2.compareTo(o1);
			}
		});
		
		for (TestCaseReadinessTimeSeriesItem item : items) {
			datesReversed.add(item.getItemCreationDate());
			if (!itemsByAlmId.containsKey(item.getAlmId())) {
				itemsByAlmId.put(item.getAlmId(), new ArrayList<TestCaseReadinessTimeSeriesItem>());
			}
			itemsByAlmId.get(item.getAlmId()).add(item);
		}
		
		for (Date date : dateSelector.filter(datesReversed)) {
			for (TestCaseReadinessTimeSeriesItem item : items) {
				if (date.equals(item.getItemCreationDate())) {
					response.increment(date, item.getStatus());
				}
			}
		}
		
		return Response.ok(response.toJson()).build();
	}
	
	@GET
	@Path("/getRemoteCredentials")
	@Produces(APPLICATION_JSON)
	public Response getRemoteCredentials() {
		UserKey userKey = userManager.getRemoteUserKey();
		UserSettings userSettings = userSettingsService.getUserSettings(userKey);
		Option<String> settings = userSettings.getString(AdminServlet.USER_SETTINGS_KEY);
		
		if (settings.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		
		return Response.ok(settings.get()).build();
	}
	
	private DataSource getDataSource(String dataSourceId) {
		try {
			PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
			JSONObject settings = new JSONObject((String) pluginSettings.get(AdminServlet.DATA_SOURCE_KEYS + ":" + dataSourceId));
			return DataSource.from(settings);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private Set<String> splitIssueKeys(String s) {
		if (s.isEmpty()) {
			return new HashSet<String>();
		}
		
		Set<String> ids = new HashSet<String>();
		
		for (String id : s.split(",")) {
			ids.add(id);
		}
		
		return ids;
	}
	
	private Set<String> getIssueKeysFromJiraProject(String jiraProjectId) throws SearchException {
		Set<String> issueKeys = new HashSet<String>();
		ApplicationUser user = jboardUserService.getLoggedInUser();
		SearchResults searchResults = searchService.search(user, "project=" + jiraProjectId);
		List<Issue> issues = searchResults.getIssues();
		for (Issue issue : issues) {
			issueKeys.add(issue.getKey());
		}

		return issueKeys;
	}

	private void filterTests(List<Test> tests, Set<String> testIds) {
		Iterator<Test> iterator = tests.iterator();
		while (iterator.hasNext()) {
			if (!testIds.contains(iterator.next().getId())) {
				iterator.remove();
			}
		}
	}

	private void filterRuns(List<Run> runs, Set<String> testIds) {
		Iterator<Run> iterator = runs.iterator();
		while (iterator.hasNext()) {
			if (!testIds.contains(iterator.next().getTestId())) {
				iterator.remove();
			}
		}
	}

	private Set<String> getTestIds(Set<String> issueKeys, HpAlmService almService, Project project) {
		Set<String> testIds = new HashSet<String>();
		for (Requirement requirement : almService.getRequirements(project)) {
			if (!issueKeys.contains(requirement.getJiraIssueKey())) {
				continue;
			}
			
			for (Test test : requirement.getTests()) {
				testIds.add(test.getId());
			}
		}
		
		return testIds;
	}
	
	private Date getFrom(String date) {
		if (date.isEmpty()) {
			return fourWeeksAgo();
		}
		
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	private Date getTo(String date) {
		if (date.isEmpty()) {
			return null;
		}
		
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	private Date fourWeeksAgo() {
		Calendar fourWeeksAgoCalendar = Calendar.getInstance();
		fourWeeksAgoCalendar.add(Calendar.WEEK_OF_YEAR, -4);
		Date fourWeeksAgo = fourWeeksAgoCalendar.getTime();
		
		return fourWeeksAgo;
	}
}