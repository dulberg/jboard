package com.bb.jboard.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.*;
import java.util.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/hardcoded")
public class HardcodedResource {
  
  private static final Random RANDOM = new Random();
  
  @GET
  @Path("/userStoryTestStatus")
  @Produces(APPLICATION_JSON)
  public Response getUserStoryTestStatus() {
    Map<String, Object> data = new HashMap<String, Object>();
    List<Map<String, Object>> series = new ArrayList<Map<String, Object>>();
    series.add(series("neverRun", "Never Run"));
    series.add(series("blocked", "Blocked"));
    series.add(series("failed", "Failed"));
    series.add(series("passed", "Passed"));
    data.put("series", series);
    data.put("data", Arrays.asList(
      story("WID-1", "User can log in", 4, 1, 3, 5),
      story("WID-2", "Admin can modify user role", 7, 5, 8, 2),
      story("WID-3", "Moderator can open thread", 0, 5, 7, 1)
    ));
    
    return Response.ok(data).build();
  }
  
  @GET
  @Path("/manualTestActivity")
  @Produces(APPLICATION_JSON)
  public Response getManualTestActivity() {
    Map<String, Object> data = new HashMap<String, Object>();
    
    List<String> dates = Arrays.asList("2016-05-15", "2016-05-16", "2016-05-17", "2016-05-18", "2016-05-19");
    List<Map<String, Object>> series = Arrays.asList(series("Activity"));
    
    data.put("dates", dates);
    data.put("series", series);
    data.put("status", "success");
    
    return Response.ok(data).build();
  }
  
  @GET
  @Path("/testPlanProgress")
  @Produces(APPLICATION_JSON)
  public Response getTestPlanProgress() {
    Map<String, Object> data = new HashMap<String, Object>();
    
    List<String> dates = Arrays.asList("2016-05-15", "2016-05-16", "2016-05-17", "2016-05-18", "2016-05-19");
    List<Map<String, Object>> series = Arrays.asList(
      series("No Run"),
      series("Failed"),
      series("Blocked"),
      series("Passed")
    );
    
    data.put("dates", dates);
    data.put("series", series);
    data.put("status", "success");
    
    return Response.ok(data).build();
  }

  @GET
  @Path("/testFailureAnalysis")
  @Produces(APPLICATION_JSON)
  public Response getTestFailureAnalysis() {
    Map<String, Object> data = new HashMap<String, Object>();
    
    List<String> dates = Arrays.asList("2016-05-15", "2016-05-16", "2016-05-17", "2016-05-18", "2016-05-19");
    List<Map<String, Object>> series = Arrays.asList(
      series("knownIssue"),
      series("newIssue"),
      series("none"),
      series("regression")
    );
    
    data.put("dates", dates);
    data.put("series", series);
    data.put("status", "success");
    
    return Response.ok(data).build();
  }
  
  @GET
  @Path("/testCaseReadiness")
  @Produces(APPLICATION_JSON)
  public Response getTestCaseReadiness() {
    Map<String, Object> data = new HashMap<String, Object>();
    
    List<String> dates = Arrays.asList("2016-05-15", "2016-05-16", "2016-05-17", "2016-05-18", "2016-05-19");
    List<Map<String, Object>> series = Arrays.asList(
      series("design"),
      series("ready")
    );
    
    data.put("dates", dates);
    data.put("series", series);
    data.put("status", "success");
    
    return Response.ok(data).build();
  }

  private Map<String, Object> series(String id, String name) {
    Map<String, Object> series = new HashMap<String, Object>();
    series.put("id", id);
    series.put("name", name);
    
    return series;
  }
  
  private Map<String, Object> series(String id) {
    Map<String, Object> series = new HashMap<String, Object>();
    series.put("id", id);
    series.put("data", new int[]{ RANDOM.nextInt(11), RANDOM.nextInt(11), RANDOM.nextInt(11), RANDOM.nextInt(11), RANDOM.nextInt(11) });
    
    return series;
  }
  
  private Map<String, Object> story(String key, String name, int neverRun, int blocked, int failed, int passed) {
    Map<String, Object> series = new HashMap<String, Object>();
    series.put("key", key);
    series.put("name", name);
    series.put("neverRun", neverRun);
    series.put("blocked", blocked);
    series.put("failed", failed);
    series.put("passed", passed);
    
    return series;
  }
}
