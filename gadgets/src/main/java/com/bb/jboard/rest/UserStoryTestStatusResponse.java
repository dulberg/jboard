package com.bb.jboard.rest;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.*;

import com.bb.jboard.hpalm.Test;
import com.bb.jboard.hpalm.Test.Status;
@XmlRootElement(name = "message")
@XmlAccessorType(FIELD)
public class UserStoryTestStatusResponse {
	
	static class Series {
		@XmlElement
		private String id;
		@XmlElement
		private String name;
		
		private Series(String name) {
			this(name.toLowerCase(), name);
		}
		
		public Series(String id, String name) {
			this.id = id;
			this.name = name;
		}
		
		public Series() {}

		static final Series NEVER_RUN = new Series("neverRun", "Never Run");
		static final Series BLOCKED = new Series("Blocked");
		static final Series FAILED = new Series("Failed");
		static final Series PASSED = new Series("Passed");
	}
	
	static class Data {
		@XmlElement
		private String name;
		@XmlElement
		private int neverRun = 0;
		@XmlElement
		private int blocked = 0;
		@XmlElement
		private int failed = 0; 
		@XmlElement
		private int passed = 0;
		@XmlElement
		private String key;
		
		public Data() {}
		
		public Data(String name, String key) {
			this.name = name;
			this.key = key;
		}

		public void add(Test test) {
			Status status = test.getExecStatus();
			
			if (status == null) {
				return;
			}
			
			switch (status) {
				case NO_RUN:
					this.neverRun++;
					break;
				case FAILED:
					this.failed++;
					break;
				case PASSED:
					this.passed++;
					break;
				case BLOCKED:
					this.blocked++;
					break;
			}
		}
	}

    @XmlElement
    private List<Series> series = Arrays.asList(Series.NEVER_RUN, Series.BLOCKED, Series.FAILED, Series.PASSED);
    
    @XmlElement(name="data")
    private List<Data> datas = new ArrayList<UserStoryTestStatusResponse.Data>();
    
    public void add(UserStoryTestStatusResponse.Data data) {
    	datas.add(data);
    }
}