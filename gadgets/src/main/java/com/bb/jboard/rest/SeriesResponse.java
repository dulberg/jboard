package com.bb.jboard.rest;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.time.FastDateFormat;

@XmlRootElement
public class SeriesResponse {
	
	static class DataPoint {
		private final String date;
		private final String id;
		private int value;

		public DataPoint(String id, String date) {
			this.id = id;
			this.date = date;
		}
	}
	
	private final List<DataPoint> dataPoints = new ArrayList<DataPoint>();
	private final Format dateFormat;
	private final String dateFormatString;

	public SeriesResponse() {
		this.dateFormat = FastDateFormat.getInstance("yyyy-MM-dd");
		this.dateFormatString = "YYYY-MM-DD";
	}
	
	public SeriesResponse(Format dateFormat) {
		this.dateFormat = dateFormat;
		this.dateFormatString = null;
	}
	
	public SeriesResponse(FastDateFormat dateFormat) {
	  this.dateFormatString = dateFormat.getPattern();
	  this.dateFormat = dateFormat;
	}

	public void increment(Date date, String id) {
		String formattedDate = dateFormat.format(date);
		for (DataPoint dataPoint : dataPoints) {
			if (dataPoint.date.equals(formattedDate) && dataPoint.id.equals(id)) {
				dataPoint.value++;
				return;
			}
		}
		
		DataPoint dataPoint = new DataPoint(id, formattedDate);
		dataPoint.value++;
		dataPoints.add(dataPoint);
	}
	
	public Map<String, ?> toJson() {
		HashMap<String, Object> json = new HashMap<String, Object>();
		
		if (dataPoints.isEmpty()) {
		  json.put("status", "no-data");
		
		  return json;
		}
		
		List<Map<String, ?>> jsonSeries = new ArrayList<Map<String,?>>();
		
		SortedSet<String> jsonDates = new TreeSet<String>();
		Set<String> jsonIds = new LinkedHashSet<String>();
		
		for (DataPoint dataPoint : dataPoints) {
			jsonDates.add(dataPoint.date);
			jsonIds.add(dataPoint.id);
		}
		
		int size = jsonDates.size();
		for (String id : jsonIds) {
			HashMap<String, Object> jsonSerie = new HashMap<String, Object>();
			jsonSerie.put("id", id);
			ArrayList<Integer> data = new ArrayList<Integer>(size);
			for (int i = 0; i < size; i++) {
				data.add(Integer.valueOf(0));
			}
			jsonSerie.put("data", data);
			jsonSeries.add(jsonSerie);
		}
		
		List<String> jsonDatesList = new ArrayList<String>(jsonDates);
		for (int i = 0; i < size; i++) {
			for (DataPoint dataPoint : dataPoints) {
				if (!jsonDatesList.get(i).equals(dataPoint.date)) {
					continue;
				}
				for (Map<String, ?> jsonSerie : jsonSeries) {
					if (jsonSerie.get("id").equals(dataPoint.id)) {
						((List<Integer>) jsonSerie.get("data")).set(i, dataPoint.value);
					}
				}
			}
		}
		
	  json.put("status", "success");
		json.put("dates", jsonDates);
		json.put("series", jsonSeries);
		json.put("dateFormat", dateFormatString);
		
		return json;
	}
}
