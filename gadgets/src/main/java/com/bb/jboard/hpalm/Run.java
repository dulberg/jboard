package com.bb.jboard.hpalm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class Run {

	private final Entity entity;
	private final Date executionDateTime;

	public Run(Entity entity) {
		this.entity = entity;
		try {
			this.executionDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(entity.getString("execution-date", null) + " " + entity.getString("execution-time", null));
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public Date getExecutionDateTime() {
		return executionDateTime;
	}
	
	public Date getExecutionDate() {
		String date = entity.getString("execution-date", null);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public String getTestId() {
		return entity.getString("test-id", null);
	}

	public String getStatus() {
		return entity.getString("status", null);
	}

}
