package com.bb.jboard.hpalm.v11_52;

import java.util.List;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;
import retrofit.http.Query;

import com.bb.jboard.hpalm.Domain;
import com.bb.jboard.hpalm.Entity;

interface HpAlmEndpointV11_52 {

	@GET("/rest/is-authenticated?login-form-required=y")
	Response isAuthenticated();
	
	@GET("/authentication-point/authenticate?login-form-required=y")
	Response authenticate(@Header("Authorization") String authorization);
	
	@GET("/authentication-point/logout")
	Response logout();

	@GET("/rest/domains?include-projects-info=y")
	List<Domain> getDomains();

	@GET("/rest/domains/{domain}/projects/{project}/defects")
	List<Entity> getDefects(@Path("domain") String domain, @Path("project") String project);

	@GET("/rest/domains/{domain}/projects/{project}/tests")
	List<Entity> getTests(@Path("domain") String domain, @Path("project") String project, @Query("fields") String fields);

	@GET("/rest/domains/{domain}/projects/{project}/tests")
	List<Entity> getTests(@Path("domain") String domain, @Path("project") String project, @Query("query") String query, @Query("fields") String fields);

	@GET("/rest/domains/{domain}/projects/{project}/test-instances")
	List<Entity> getTestInstances(@Path("domain") String domain, @Path("project") String project, @Query("query") String query, @Query("fields") String fields);

	@GET("/rest/domains/{domain}/projects/{project}/requirements")
	List<Entity> getRequirements(@Path("domain") String domain, @Path("project") String project, @Query("fields") String fields);
	
	@GET("/rest/domains/{domain}/projects/{project}/runs")
	List<Entity> getRuns(@Path("domain") String domain, @Path("project") String project, @Query("query") String query, @Query("fields") String fields, @Query("order-by") String orderBy);
	
	@GET("/rest/domains/{domain}/projects/{project}/requirement-coverages")
	List<Entity> getRequirementCoverages(@Path("domain") String domain, @Path("project") String project, @Query("fields") String fields);

	@GET("/rest/domains/{domain}/projects/{project}/{path}")
	Response request(@Path("domain") String domain, @Path("project") String project, @Path("path") String path);

	@GET("/rest/domains/{domain}/projects/{project}/{path1}/{path2}")
	Response request(@Path("domain") String domain, @Path("project") String project, @Path("path1") String path, @Path("path2") String path2);
}
