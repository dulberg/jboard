package com.bb.jboard.hpalm.v11_52;

import java.text.Format;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

import com.bb.jboard.admin.DataSource;
import com.bb.jboard.hpalm.Domain;
import com.bb.jboard.hpalm.Entity;
import com.bb.jboard.hpalm.HpAlmService;
import com.bb.jboard.hpalm.Project;
import com.bb.jboard.hpalm.Requirement;
import com.bb.jboard.hpalm.Run;
import com.bb.jboard.hpalm.Test;
import com.bb.jboard.hpalm.TestInstance;

public class HpAlmServiceV11_52 implements HpAlmService {
	
	private final HpAlmEndpointV11_52 authenticationEndpoint;
	private final String username;
	private final String password;
	private HpAlmEndpointV11_52 endpoint;
	private final String baseUrl;
	private final DataSource dataSource;
	
	public HpAlmServiceV11_52(DataSource dataSource) {
		this.dataSource = dataSource;
		this.baseUrl = dataSource.getUrl();
		this.username = dataSource.getUser();
		this.password = dataSource.getPassword();
		this.authenticationEndpoint = new RestAdapter.Builder()
			.setEndpoint(baseUrl)
//			.setLogLevel(RestAdapter.LogLevel.FULL)
			.setRequestInterceptor(new RequestInterceptor() {
				@Override
				public void intercept(RequestFacade request) {
					request.addHeader("Accept", "application/xml");
				}
			})
			.build()
			.create(HpAlmEndpointV11_52.class);
	}

	@Override
	public void authenticate() {
        Response response;
		try {
			response = authenticationEndpoint.isAuthenticated();
		} catch (RetrofitError e) {
		  e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		for (Header header : response.getHeaders()) {
			if ("WWW-Authenticate".equals(header.getName())) {
				header.getValue();
			}
		}
		
		String authorization = Base64.encodeBase64URLSafeString((username + ":" + password).getBytes());
		Response authenticationResponse = authenticationEndpoint.authenticate("Basic " + authorization);
		String lwsso = null;
		final List<Header> cookies = new ArrayList<Header>();
		for (final Header header : authenticationResponse.getHeaders()) {
			if ("Set-Cookie".equals(header.getName())) {
//				lwsso = header.getValue().substring(17);
				cookies.add(header);
			}
		}
		
//		if (lwsso == null) {
//			return;
//		}
		
//		final String lwssoCookieKey = lwsso.substring(0, lwsso.indexOf(';'));
		
		endpoint = new RestAdapter.Builder()
			.setRequestInterceptor(new RequestInterceptor() {
				@Override
				public void intercept(RequestFacade request) {
//					request.addHeader("Set-Cookie", "LWSSO_COOKIE_KEY=" + lwssoCookieKey);
					request.addHeader("Accept", "application/xml");
					for (Header cookie : cookies) {
						request.addHeader("Cookie", cookie.getValue());
					}
				}
			})
			.setConverter(new SafeSaxConverter())
			.setEndpoint(baseUrl)
			.setLogLevel(RestAdapter.LogLevel.FULL)
			.build()
			.create(HpAlmEndpointV11_52.class);
	}
	
	@Override
	public List<Domain> getDomains() {
		return endpoint.getDomains();
	}
	
	@Override
	public Project getProject(DataSource dataSource) {
		List<Domain> domains = getDomains();
		
		for (Domain domain : domains) {
			if (!domain.getName().equalsIgnoreCase(dataSource.getDomain())) {
				continue;
			}
			for (Project project : domain.getProjects()) {
				if (project.getName().equalsIgnoreCase(dataSource.getProject())) {
					return project;
				}
			}
		}
		
		throw new RuntimeException("No project found! Domain = " + dataSource.getDomain() + " / Project = " + dataSource.getProject());
	}
	
	@Override
	public List<Test> getRecentTests(Project project, String query) {
		String fullQuery = "{" + query + (!query.isEmpty() ? ";" : "") + "last-modified[>=" + fourWeeksAgo() + "]}";
		
		return getTests(project, fullQuery);
	}
	
	@Override
	public List<Test> getTests(Project project, String query) {
		String fields = "exec-status,last-modified,status";
		List<Entity> entities = query.isEmpty() ? endpoint.getTests(project.getDomain().getName(), project.getName(), fields) : endpoint.getTests(project.getDomain().getName(), project.getName(), query, fields);
		List<Test> tests = new ArrayList<Test>();
		
		for (Entity entity : entities) {
			tests.add(new Test(entity));
		}
		
		return tests;
	}
	
	@Override
	public List<Test> getTests(Date from, Date to, String query, Project project) {
		Format format = DateFormatUtils.ISO_DATE_FORMAT;
		String _query = "{";
		if (from != null) {
			_query += "last-modified[>=" + format.format(from) + "]";
			if (to != null) {
				_query += ";";
			}
		}
		if (to != null) {
			_query += "last-modified[<" + format.format(to) + "]"; 
			if (!query.isEmpty()) {
				_query += ";";
			}
		}
		_query += query + "}";
		
		return getTests(project, _query);
	}
	
	@Override
	public List<TestInstance> getTestInstances(Date from, Date to, Project project, Set<String> issueIds) {
		Format format = DateFormatUtils.ISO_DATE_FORMAT;
		String query = "{";
		if (from != null) {
			query += "exec-date[>=" + format.format(from) + "]";
			if (to != null) {
				query += ";";
			}
		}
		if (to != null) {
			query += "exec-date[<=" + format.format(to) + "]";
		}
		query += "}";
		
		List<Entity> entities = endpoint.getTestInstances(project.getDomain().getName(), project.getName(), query, "exec-date,status,test-id");
		
		List<TestInstance> testInstances = new ArrayList<TestInstance>();
		
		for (Entity entity : entities) {
			testInstances.add(new TestInstance(entity));
		}
		
		if (!issueIds.isEmpty()) {
			Set<String> testIds = new HashSet<String>();
			for (Requirement requirement : getRequirements(project)) {
				if (!issueIds.contains(requirement.getJiraIssueKey())) {
					continue;
				}
				
				for (Test test : requirement.getTests()) {
					testIds.add(test.getId());
				}
			}
			
			Iterator<TestInstance> iterator = testInstances.iterator();
			while (iterator.hasNext()) {
				if (!testIds.contains(iterator.next().getTestId())) {
					iterator.remove();
				}
			}
		}
		
		return testInstances;
	}
	
	@Override
	public List<Requirement> getRequirements(Project project) {
		List<Entity> entities = endpoint.getRequirements(project.getDomain().getName(), project.getName(), "id,name," + dataSource.getJiraIdKey());
		List<Entity> requirementCoverages = endpoint.getRequirementCoverages(project.getDomain().getName(), project.getName(), "requirement-id,test-id");
		List<Test> allTests = getTests(project, "");
		List<Requirement> requirements = new ArrayList<Requirement>(entities.size());
		for (Entity requirementEntity : entities) {
			List<Test> tests = new ArrayList<Test>();
			for (Entity requirementCoverage : requirementCoverages) {
				if (requirementEntity.getString("id", "NONE").equals(requirementCoverage.getString("requirement-id", null))) {
					for (Test test : allTests) {
						if (StringUtils.equals(test.getId(), requirementCoverage.getString("test-id", "requirementCoverage.ID.NONE"))) {
							tests.add(test);
						}
					}
				}
			}
			requirements.add(new Requirement(requirementEntity, tests, dataSource));
		}
		
		return requirements;
	}
	
	@Override
	public List<Run> getRuns(Date from, Date to, String query, Project project) {
		Format format = DateFormatUtils.ISO_DATE_FORMAT;
		String _query = "{";
		if (from != null) {
			_query += "execution-date[>=" + format.format(from) + "]";
			if (to != null) {
				_query += ";";
			}
		}
		if (to != null) {
			_query += "execution-date[<" + format.format(to) + "]"; 
		}
		if (!query.isEmpty() && (from != null || to != null)) {
			_query += ";";
		}
		_query += query + "}";

		List<Entity> entities = endpoint.getRuns(project.getDomain().getName(), project.getName(), _query, "test-id,execution-date,execution-time,status", "{test-id;execution-date[DESC];execution-time[DESC]}");
		List<Run> runs = new ArrayList<Run>(entities.size());
		for (Entity entity : entities) {
			runs.add(new Run(entity));
		}
		
		return runs;
	}
	
	public Response getResponse(Domain domain, Project project, String path) {
		return endpoint.request(project.getDomain().getName(), project.getName(), path);
	}
	
	public Response getResponse(Project project, String path1, String path2) {
		return endpoint.request(project.getDomain().getName(), project.getName(), path1, path2);
	}
	
	@Override
	public void logout() {
		endpoint.logout();
	}

	private String fourWeeksAgo() {
		Calendar fourWeeksAgoCalendar = Calendar.getInstance();
		fourWeeksAgoCalendar.add(Calendar.WEEK_OF_YEAR, -4);
		Date fourWeeksAgo = fourWeeksAgoCalendar.getTime();
		
		return DateFormatUtils.ISO_DATE_FORMAT.format(fourWeeksAgo);
	}
}
