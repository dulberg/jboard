package com.bb.jboard.hpalm;

import java.util.List;

import com.bb.jboard.admin.DataSource;

public class Requirement {

	private final Entity entity;
	private final List<Test> tests;
	private String jiraIssueFieldName;

	public Requirement(Entity entity, List<Test> tests, DataSource dataSource) {
		this.entity = entity;
		this.tests = tests;
		this.jiraIssueFieldName = dataSource.getJiraIdKey();
	}
	
	public String getJiraIssueKey() {
		return entity.getString(jiraIssueFieldName, null);
	}

	public String getName() {
		return entity.getString("name", null);
	}

	public List<Test> getTests() {
		return tests;
	}
}
