package com.bb.jboard.hpalm;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jboard.org.apache.commons.lang3.time.FastDateParser;

public class Entity {
	private static final FastDateParser DATE_TIME_FORMAT = FastDateParser.HP_ENTITY_DATE_TIME_FORMAT;

	private final String type;
	private final List<Field> fields = new ArrayList<Field>();
	
	public Entity(String type) {
		this.type = type;
	}
	
	public void add(Field field) {
		fields.add(field);
	}

	public String getString(String name, String defaultValue) {
		for (Field field : fields) {
			if (field.getName().equals(name)) {
				Object value = field.getValue();
				
				return value != null ? (String) value : defaultValue;
			}
		}
		
		return defaultValue;
	}
	
	public Date getDateTime(String name, Date defaultValue) {
		String date = getString(name, null);
		if (date == null) {
			return defaultValue;
		}
		
		try {
			return DATE_TIME_FORMAT.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String toString() {
		return "<Entity>" + fields.toString() + "</Entity>";
	}
}
