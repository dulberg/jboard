package com.bb.jboard.hpalm;

import java.util.ArrayList;
import java.util.List;

public class Domain {

	private final String Name;
	private final List<Project> projects = new ArrayList<Project>();

	public Domain(String name) {
		this.Name = name;
	}

	public void add(Project project) {
		projects.add(project);
	}
	
	public String getName() {
		return Name;
	}
	
	public List<Project> getProjects() {
		return projects;
	}
}
