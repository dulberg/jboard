package com.bb.jboard.hpalm;

import java.util.ArrayList;
import java.util.List;

public class Field {

	private String name;
	
	private List<Value> values = new ArrayList<Value>();
	
	public Field(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Object getValue() {
		if (values == null || values.isEmpty()) {
			return null;
		}
		
		return values.get(0).get();
	}
	
	public void add(Value value) {
		values.add(value);
	}
	
	@Override
	public String toString() {
		return "Field: name = " + name + " " + values.toString();
	}
	
	
}
