package com.bb.jboard.hpalm.v11_52;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;
import safesax.Element;
import safesax.ElementListener;
import safesax.EndTextElementListener;
import safesax.RootElement;
import safesax.StartElementListener;

import com.bb.jboard.hpalm.Domain;
import com.bb.jboard.hpalm.Entity;
import com.bb.jboard.hpalm.Field;
import com.bb.jboard.hpalm.Project;
import com.bb.jboard.hpalm.Value;

public class SafeSaxConverter implements Converter {

	@Override
	public Object fromBody(TypedInput body, Type type) throws ConversionException {
		if (type instanceof ParameterizedType && ((ParameterizedType) type).getActualTypeArguments()[0] == Domain.class) {
			return readDomains(body);
		}
		
		return readEntities(body);
	}

	private Object readEntities(TypedInput body) throws ConversionException {
		final List<Entity> entities = new ArrayList<Entity>();
		final AtomicReference<Entity> entity = new AtomicReference<Entity>();
		final AtomicReference<Field> field = new AtomicReference<Field>();
		RootElement root = new RootElement("Entities");
		Element entityElement = root.getChild("Entity");
		Element fieldElement = entityElement.getChild("Fields").getChild("Field");
		Element valueElement = fieldElement.getChild("Value");
		
		entityElement.setStartElementListener(new StartElementListener() {
			@Override
			public void start(Attributes attributes) {
				entity.set(new Entity(attributes.getValue("Type")));
				entities.add(entity.get());
			}
		});
		fieldElement.setElementListener(new ElementListener() {
			@Override
			public void start(Attributes attributes) {
				field.set(new Field(attributes.getValue("Name")));
				entity.get().add(field.get());
			}
			
			@Override
			public void end() {
			}
		});
		valueElement.setEndTextElementListener(new EndTextElementListener() {
			@Override
			public void end(String body) {
				field.get().add(new Value(body));
			}
		});
		
		try {
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();
			xmlReader.setContentHandler(root.getContentHandler());
			xmlReader.parse(new InputSource(body.in()));
			
			return entities;
		} catch (Exception e) {
			throw new ConversionException(e);
		}
	}
	
	private List<Domain> readDomains(TypedInput body) {
		RootElement root = new RootElement("Domains");
		Element domainElement = root.getChild("Domain");
		Element projectElement = domainElement.getChild("Projects").getChild("Project");

		final List<Domain> domains = new ArrayList<Domain>();
		final AtomicReference<Domain> domain = new AtomicReference<Domain>();
		
		domainElement.setStartElementListener(new StartElementListener() {
			@Override
			public void start(Attributes attributes) {
				domain.set(new Domain(attributes.getValue("Name")));
				domains.add(domain.get());
			}
		});
		projectElement.setStartElementListener(new StartElementListener() {
			@Override
			public void start(Attributes attributes) {
				domain.get().add(new Project(attributes.getValue("Name"), domain.get()));
			}
		});

		try {
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();
			xmlReader.setContentHandler(root.getContentHandler());
			xmlReader.parse(new InputSource(body.in()));
			
			return domains;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public TypedOutput toBody(Object object) {
		throw new UnsupportedOperationException("Cannot serialise objects to XML, use JAX-RS support instead");
	}

}
