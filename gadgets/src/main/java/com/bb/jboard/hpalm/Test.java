package com.bb.jboard.hpalm;

import java.util.Date;


public class Test {
	
	public static enum Status {
		PASSED("Passed"), FAILED("Failed"), BLOCKED("Blocked"), NO_RUN("No Run"), NOT_COMPLETED("Not Completed"), N_A("Not Applicable");
		
		private final String xmlValue;

		private Status(String xmlValue) {
			this.xmlValue = xmlValue;
		}
		
		public static Test.Status fromXml(String xmlValue) {
			for (Status status : Status.values()) {
				if (status.xmlValue.equals(xmlValue)) {
					return status;
				}
			}
			
			throw new IllegalArgumentException("Unknown Test Status: " + xmlValue);
		}

		public String getId() {
			switch (this) {
				case NO_RUN:
					return "neverRun";
				case NOT_COMPLETED:
					return "notCompleted";
				default:
					return name().toLowerCase();
			}
		}
		
		public String getXmlValue() {
			return xmlValue;
		}
	}

	private Entity entity;
	
	public Test(Entity entity) {
		this.entity = entity;
	}
	
	public String getId() {
		return entity.getString("id", null);
	}
	
	public String getStatus() {
		return entity.getString("status", "unknown");
	}

	public Test.Status getExecStatus() {
		String status = entity.getString("exec-status", null);
		if (status == null) {
			return Status.NOT_COMPLETED;
		}
		
		return Status.fromXml(status);
	}

	public Date getLastModifiedDate() {
		return entity.getDateTime("last-modified", null);
	}
}
