package com.bb.jboard.hpalm;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bb.jboard.admin.DataSource;

public interface HpAlmService {

	void authenticate();
	void logout();
	List<Domain> getDomains();
	Project getProject(DataSource dataSource);
	List<Test> getRecentTests(Project project, String query);
	List<Test> getTests(Project project, String query);
	List<Test> getTests(Date from, Date to, String query, Project project);
	List<TestInstance> getTestInstances(Date from, Date to, Project project, Set<String> issueIds);
	List<Requirement> getRequirements(Project project);
	List<Run> getRuns(Date from, Date to, String query, Project project);
}
