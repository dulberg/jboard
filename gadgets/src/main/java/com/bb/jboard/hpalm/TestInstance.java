package com.bb.jboard.hpalm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestInstance {

	private final Entity entity;

	public TestInstance(Entity entity) {
		this.entity = entity;
	}

	public Date getExecDate() {
		String dateString = entity.getString("exec-date", null);
		String timeString = entity.getString("exec-time", null);
		
		if (timeString == null) {
			try {
				return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
			} catch (ParseException e) {
				return null;
			}
		}
		
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString + " " + timeString);
		} catch (ParseException e) {
			return null;
		}
	}

	public String getStatus() {
		return entity.getString("status", null);
	}

	public String getTestId() {
		return entity.getString("test-id", null);
	}

}
