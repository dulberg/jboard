package com.bb.jboard.hpalm;


public class Value {

	private String alias;
	private String referenceValue;
	private String value;

	public Value(String value) {
		this.value = value;
	}

	public Object get() {
		return value;
	}
	
	@Override
	public String toString() {
		return "<Value = " + value + "; @alias=" + alias + "; @referenceValue=" + referenceValue + ">";
	}
}
