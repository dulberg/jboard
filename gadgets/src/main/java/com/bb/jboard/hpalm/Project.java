package com.bb.jboard.hpalm;

public class Project {

	private String name;
	private Domain domain;
	
	public Project(String name, Domain domain) {
		this.name = name;
		this.domain = domain;
	}

	public String getName() {
		return name;
	}

	public Domain getDomain() {
		return domain;
	}
}
