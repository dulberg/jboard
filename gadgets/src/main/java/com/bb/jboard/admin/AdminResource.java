package com.bb.jboard.admin;

import static com.bb.jboard.admin.AdminServlet.DATA_SOURCE_KEYS;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import jboard.timeseries.TestCaseReadinessSchedulerComponent;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.bb.jboard.hpalm.HpAlmService;
import com.bb.jboard.hpalm.v11_52.HpAlmServiceV11_52;

@Path("/admin")
public class AdminResource {

    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;
	private final PluginSettingsFactory pluginSettingsFactory;
	private final URI adminPageUri;
	private final TestCaseReadinessSchedulerComponent testCaseReadinessScheduler;

	public AdminResource(TestCaseReadinessSchedulerComponent testCaseReadinessScheduler, UserManager userManager, LoginUriProvider loginUriProvider, PluginSettingsFactory pluginSettingsFactory, ApplicationProperties applicationProperties) {
		this.testCaseReadinessScheduler = testCaseReadinessScheduler;
		this.userManager = userManager;
		this.loginUriProvider = loginUriProvider;
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.adminPageUri = UriBuilder.fromUri(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE)).segment("plugins", "servlet", "jgbe", "admin").build();
	}
	
	@GET
	@Path("/dataSources")
	@Produces(APPLICATION_JSON)
	public List<DataSource> getDataSources() {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        
        try {
        	String dataSourceKeysString = (String) pluginSettings.get(DATA_SOURCE_KEYS);
        	JSONArray dataSourceKeys = dataSourceKeysString != null ? new JSONArray(dataSourceKeysString) : new JSONArray();
        	List<DataSource> dataSources = new ArrayList<DataSource>();
        	
        	for (int i = 0; i < dataSourceKeys.length(); i++) {
        		String id = dataSourceKeys.getString(i);
        		JSONObject dataSource = new JSONObject((String) pluginSettings.get(DATA_SOURCE_KEYS + ":" + id));
        		DataSource reducedDataSource = DataSource.reducedFrom(dataSource);
        		dataSources.add(reducedDataSource);
        	}
        	
        	return dataSources;
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}
	
	@POST
	@Path("/dataSources")
	public Response createDataSource(@FormParam("displayName") String displayName, @FormParam("url") String url, @FormParam("domain") String domain, @FormParam("project") String project, @FormParam("user") String user, @FormParam("password") String password, @FormParam("jiraKeyId") String jiraKeyId) throws JSONException {
    	UserKey userKey = userManager.getRemoteUserKey();
        if (userKey == null || !userManager.isSystemAdmin(userKey)) {
            return Response.temporaryRedirect(loginUriProvider.getLoginUri(adminPageUri)).build();
        }

        JSONObject dataSource = new JSONObject();
		String id = UUID.randomUUID().toString();
		dataSource.put("id", id);
		dataSource.put("displayName", displayName);
		dataSource.put("url", url);
		dataSource.put("domain", domain);
		dataSource.put("project", project);
		dataSource.put("user", user);
		dataSource.put("password", password);
		dataSource.put("type", "HP ALM");
		dataSource.put("jiraKeyId", jiraKeyId);
		
		PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
		pluginSettings.put(AdminServlet.DATA_SOURCE_KEYS + ":" + id, dataSource.toString());
		String dataSourceKeysString = (String) pluginSettings.get(AdminServlet.DATA_SOURCE_KEYS);
		JSONArray dataSourceKeys = dataSourceKeysString != null ? new JSONArray(dataSourceKeysString) : new JSONArray();
		dataSourceKeys.put(id);
		pluginSettings.put(AdminServlet.DATA_SOURCE_KEYS, dataSourceKeys.toString());
		
		testCaseReadinessScheduler.updateJob();
		
		return redirectToAdminPage();
	}
	
	@POST
	@Path("/dataSources/{id}")
	public Response updateDataSource(@PathParam("id") String id, @FormParam("displayName") String displayName, @FormParam("url") String url, @FormParam("domain") String domain, @FormParam("project") String project, @FormParam("user") String user, @FormParam("password") String password, @FormParam("jiraKeyId") String jiraKeyId) throws JSONException {
    	UserKey userKey = userManager.getRemoteUserKey();
        if (userKey == null || !userManager.isSystemAdmin(userKey)) {
            return Response.temporaryRedirect(loginUriProvider.getLoginUri(adminPageUri)).build();
        }
        
        String key = DATA_SOURCE_KEYS + ":" + id;
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        String dataSourceSettings = (String) pluginSettings.get(key);
        
        if (dataSourceSettings == null) {
        	return redirectToAdminPage();
        }
        
		JSONObject dataSource = new JSONObject(dataSourceSettings);
        dataSource.put("displayName", displayName);
        dataSource.put("url", url);
        dataSource.put("domain", domain);
		dataSource.put("project", project);
		dataSource.put("user", user);
		dataSource.put("password", password);
		dataSource.put("jiraKeyId", jiraKeyId);
		pluginSettings.put(key, dataSource.toString());
		
		testCaseReadinessScheduler.updateJob();
		
		return redirectToAdminPage();
	}
	
	@GET
	@Path("/dataSources/{id}/test")
	@Produces(APPLICATION_JSON)
	public Map<String, Object> testDataSource(@PathParam("id") String id) throws Exception {
		HashMap<String, Object> response = new HashMap<String, Object>();
		DataSource dataSource = DataSource.from(getDataSource(id));
		HpAlmService almService = new HpAlmServiceV11_52(dataSource);
		
		try {
			almService.authenticate();
			almService.getProject(dataSource);
			response.put("result", "success");
		} catch (Exception e) {
			response.put("result", "failure");
			response.put("message", e.getMessage());
		}
		
		return response;
	}
	
	@POST
	@Path("/settings")
	public Response updateSettings(@FormParam("refreshInterval") String refreshInterval) {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        
        Object existingTimeSeriesPollInterval = pluginSettings.get(AdminServlet.REFRESH_INTERVAL_SETTINGS_KEY);
		if (existingTimeSeriesPollInterval == null || !existingTimeSeriesPollInterval.equals(refreshInterval)) {
        	pluginSettings.put(AdminServlet.REFRESH_INTERVAL_SETTINGS_KEY, refreshInterval);
        	testCaseReadinessScheduler.updateJob();
        }
		
		return redirectToAdminPage();
	}
	
	@GET
	@Path("/verify")
	public Response verify() {
	  return Response.ok().build();
	}
	
	@GET
	@Path("/globalSettings")
    @Produces(APPLICATION_JSON)
	public Response getSettings() {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        
        Object dataCollectionInterval = pluginSettings.get(AdminServlet.REFRESH_INTERVAL_SETTINGS_KEY);
        
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("dataCollectionInterval", dataCollectionInterval);
		
		return Response.ok(response).build();
	}
	
	private JSONObject getDataSource(String id) throws JSONException {
        String key = DATA_SOURCE_KEYS + ":" + id;
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        String dataSourceSettings = (String) pluginSettings.get(key);
        
        if (dataSourceSettings == null) {
        	return null;
        }
        
		return new JSONObject(dataSourceSettings);
	}
	
	private Response redirectToAdminPage() {
		return Response.seeOther(adminPageUri).build();
	}
}
