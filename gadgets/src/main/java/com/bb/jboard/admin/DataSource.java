package com.bb.jboard.admin;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections.map.MultiValueMap;

import com.atlassian.jira.util.json.JSONObject;

@XmlRootElement
public class DataSource {
	
	@XmlElement
	private String id;
	@XmlElement
	private String displayName;
	@XmlElement
	private String url;
	@XmlElement
	private String user;
	@XmlElement
	private String password;
	@XmlElement
	private String domain;
	@XmlElement
	private String project;
	@XmlElement
	private String type;
	@XmlElement
	private String jiraIdKey;

	public static DataSource reducedFrom(JSONObject json) {
		DataSource dataSource = new DataSource();
		dataSource.id = json.optString("id");
		dataSource.displayName = json.optString("displayName");
		
		return dataSource;
	}
	
	public static DataSource from(JSONObject json) {
		DataSource dataSource = new DataSource();
		dataSource.id = json.optString("id");
		dataSource.displayName = json.optString("displayName");
		dataSource.url = json.optString("url");
		dataSource.user = json.optString("user");
		dataSource.password = json.optString("password");
		dataSource.domain = json.optString("domain");
		dataSource.project = json.optString("project");
		dataSource.type = json.optString("type");
		dataSource.jiraIdKey = json.optString("jiraKeyId");
		
		return dataSource;
	}
	
	public static DataSource from(MultiValueMap form) {
		DataSource dataSource = new DataSource();
		dataSource.id = (String) form.get("id");
		dataSource.displayName = (String) form.get("displayName");
		dataSource.url = (String) form.get("url");
		dataSource.user = (String) form.get("user");
		dataSource.password = (String) form.get("password");
		dataSource.domain = (String) form.get("domain");
		dataSource.project = (String) form.get("project");
		dataSource.type = (String) form.get("type");
		dataSource.jiraIdKey = (String) form.get("jiraKeyId");
		
		return dataSource;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getUrl() {
		return url;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getDomain() {
		return domain;
	}

	public String getProject() {
		return project;
	}

	public String getJiraIdKey() {
		return jiraIdKey;
	}
}
