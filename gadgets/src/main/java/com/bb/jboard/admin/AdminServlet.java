package com.bb.jboard.admin;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jboard.timeseries.TestCaseReadinessSchedulerComponent;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

//@Path("/admin")
public class AdminServlet extends HttpServlet
{
	
	public static final String USER_SETTINGS_KEY = "com.bb.jboard:user";
	public static final String ALM_SETTINGS_KEY = "com.bb.jboard:alm";
	public static final String ALM_URL_KEY = ALM_SETTINGS_KEY + "Url";
	public static final String ALM_DISPLAY_NAME_KEY = ALM_SETTINGS_KEY + "DisplayName";
	public static final String DATA_SOURCE_KEYS = "com.bb.jboard:dataSources";
	public static final String REFRESH_INTERVAL_SETTINGS_KEY = "com.bb.jboard:settings:refreshInterval";
//	private static final Gson GSON = new Gson();
    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;
    private final TemplateRenderer renderer;
	private final PluginSettingsFactory pluginSettingsFactory;


    public AdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, PluginSettingsFactory pluginSettingsFactory, TemplateRenderer renderer)
    {
		this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
		this.pluginSettingsFactory = pluginSettingsFactory;
        this.renderer = renderer;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
    	UserKey userKey = userManager.getRemoteUserKey(request);
        if (userKey == null || !userManager.isSystemAdmin(userKey)) {
            redirectToLogin(request, response);
            return;
        }
        
        
        if (request.getParameter("dataSourceId") != null) {
        	renderDataSourceDetail(request.getParameter("dataSourceId"), response);
        } else {
        	renderDataSourceList(response);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	UserKey userKey = userManager.getRemoteUserKey(request);
        if (userKey == null || !userManager.isSystemAdmin(userKey)) {
            redirectToLogin(request, response);
            return;
        }
    	
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        Map<String, Object> settings = new HashMap<String, Object>();
        settings.put(ALM_URL_KEY, request.getParameter("url"));
        settings.put(ALM_DISPLAY_NAME_KEY, request.getParameter("displayName"));
        
        System.out.println("AdminServlet.doPost(): settings = " + settings);
        System.out.println("AdminServlet.doPost(): json2 = " + new JSONObject(settings));
        
        JSONObject json = new JSONObject(settings);
        Iterator<String> iterator = json.keys();
        while (iterator.hasNext()) {
        	String key = iterator.next();
			try {
				System.out.println(key + " = " + json.get(key));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		pluginSettings.put(ALM_SETTINGS_KEY, json.toString());
		
		// TODO: add datasource to scheduled jobs
        
        response.sendRedirect(getUri(request).toASCIIString());
    }
    
    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

	private void renderDataSourceList(HttpServletResponse response) throws IOException {
		HashMap<String, Object> templateData = new HashMap<String, Object>();
		PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
		templateData.put("timeSeriesPollingInterval", pluginSettings.get(REFRESH_INTERVAL_SETTINGS_KEY));
		
		try {
			String dataSourceKeysString = (String) pluginSettings.get(DATA_SOURCE_KEYS);
			JSONArray dataSourceKeys = dataSourceKeysString != null ? new JSONArray(dataSourceKeysString) : new JSONArray();
			List<JSONObject> dataSources = new ArrayList<JSONObject>();
			
			for (int i = 0; i < dataSourceKeys.length(); i++) {
				String id = dataSourceKeys.getString(i);
				JSONObject dataSource = new JSONObject((String) pluginSettings.get(DATA_SOURCE_KEYS + ":" + id));
				dataSources.add(dataSource);
			}
			
			Collections.sort(dataSources, new Comparator<JSONObject>() {
				@Override
				public int compare(JSONObject o1, JSONObject o2) {
					return o1.optString("displayName").compareTo(o2.optString("displayName"));
				}
			});
			
			templateData.put("dataSources", dataSources);
			
			response.setContentType("text/html;charset=utf-8");
			renderer.render("admin.vm", templateData, response.getWriter());
		
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}
    
    private void renderDataSourceDetail(String dataSourceId, HttpServletResponse response) {
        HashMap<String, Object> templateData = new HashMap<String, Object>();
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();

        try {
    		templateData.put("dataSource", new JSONObject((String) pluginSettings.get(DATA_SOURCE_KEYS + ":" + dataSourceId)));
    		
            response.setContentType("text/html;charset=utf-8");
            renderer.render("adminDataSourceDetail.vm", templateData, response.getWriter());
    	} catch (Exception e) {
    		throw new RuntimeException(e);
    	}
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
